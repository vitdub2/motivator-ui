//swEnvBuild.js - script that is separate from webpack
require('dotenv').config({path: '.env'}); // make sure you have '.env' file in pwd
const fs = require('fs');

fs.writeFileSync('public/swenv.js',
    `
const process = {
  env: {
    REACT_APP_API_KEY: "${process.env.REACT_APP_API_KEY}",
    REACT_APP_AUTH_DOMAIN: "${process.env.REACT_APP_AUTH_DOMAIN}",
    REACT_APP_DATABASE_URL: "${process.env.REACT_APP_DATABASE_URL}",
    REACT_APP_PROJECT_ID: "${process.env.REACT_APP_PROJECT_ID}",
    REACT_APP_STORAGE_BUCKET: "${process.env.REACT_APP_STORAGE_BUCKET}",
    REACT_APP_SENDER_ID: "${process.env.REACT_APP_SENDER_ID}",
    REACT_APP_APP_ID: "${process.env.REACT_APP_APP_ID}",
    REACT_APP_MEASUREMENT_ID: "${process.env.REACT_APP_MEASUREMENT_ID}",
    REACT_APP_VAPID_KEY: "${process.env.REACT_APP_VAPID_KEY}",
  }
}
`);
