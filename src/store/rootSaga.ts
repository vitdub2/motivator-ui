import { fork } from "redux-saga/effects";
import {watchErrorsActions} from "../modules/errors/store/saga";
import {watchAuthActions} from "../modules/auth/store/saga";
import {watchDevicesActions} from "../modules/devices/store/saga";
import {watchTasksAction} from "../modules/tasks/store/saga";

/*
* Здесь регистрируются саги
*
* Сага - асинхронный обработчик действий
*
* */
export function* rootSaga() {
  yield fork(watchErrorsActions);
  yield fork(watchAuthActions);
  yield fork(watchDevicesActions);
  yield fork(watchTasksAction);
}
