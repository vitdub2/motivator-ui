import {ILoaderState} from "../modules/loader/store/ducks";
import {IAuthState} from "../modules/auth/store/interfaces";
import {IModalsState} from "../modules/modals/store/interfaces";
import {IDeviceState} from "../modules/devices/store/interfaces";
import {ITaskState} from "../modules/tasks/store/interfaces";

//Здесь описан интерфейс store (cм. rootReducer); Нужно для selector`ов
export interface IAppState {
    loader: ILoaderState,
    auth: IAuthState,
    modals: IModalsState,
    devices: IDeviceState,
    tasks: ITaskState,
}

export interface IResponse<D, M = any> {
    data: D;
    meta: M;
}

export type PropertyName = string | number | symbol;

export type Obj<K extends PropertyName = PropertyName, V = any> = {
    [key in K]: V;
};

export type PartialObj<K extends PropertyName = PropertyName, V = any> = {
    [key in K]?: V;
};

export interface ArrayDate {
    date: string;
}

export type DateFormatFns =
    | "dd.MM.yyyy"
    | "dd.MM.yy"
    | "dd MMMM"
    | "d MMMM"
    | "dd MMMM, EEEEEE"
    | "LLLL"
    | "dd"
    | "yyyyMMdd"
    | string;

export type DateFns = Date | number | string | null | undefined;
