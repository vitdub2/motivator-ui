const withApiUrl = (url: string): string => `${process.env.REACT_APP_API_ENDPOINT}${url}`;

//Здесь описаны URLы для общения с API
export const API_URLS = {
    AUTH: {
        AUTH: withApiUrl('/auth'),
        LOGIN: withApiUrl('/auth/login'),
        PROFILE: withApiUrl('/auth/profile'),
    },
    DEVICES: withApiUrl('/devices'),
    USERS: withApiUrl('/users'),
    TASKS: withApiUrl('/tasks'),
};

//Здесь описаны URL`ы внутри приложения
export const URLS = {
    MAIN: "/",
    CONFIRM: "/confirm",
    DEVICES: "/devices",
    TASKS: "/tasks",
};

//Типы модалей
export const MODAL_TYPE = {
    AUTH: "AUTH",
    REGISTER: "REGISTER",
    CONFIRM_EMAIL: "CONFIRM_EMAIL",
    TASKS_WARNING: "TASKS_WARNING",
    TASK_CREATE: "TASK_CREATE",
    TASK_EDIT: "TASK_EDIT",
};

export const REG_EXP = {
    WORD: /[а-яА-ЯёЁa-zA-Z]*/,
    WORDS: /[а-яА-ЯёЁa-zA-Z\s]*/,
    WORDS_OR_BLANK: /[\s-]|[а-яА-ЯёЁa-zA-Z]*/g,
    EMAIL: /.+@.+\..+/,
    DATE: /(0[1-9]|[12][0-9]|3[01])[- .](0[1-9]|1[012])[- .](19|20)\d\d/,
    NUMBER: /\d/g,
};

export const MASK = {
    TIME: (value: string) => {
        const chars = value.split('');
        const hours = [
            /[0-2]/,
            chars[0] === '2' ? /[0-3]/ : /[0-9]/
        ];

        const minutes = [ /[0-5]/, /[0-9]/ ];

        return [...hours, ':', ...minutes];
    }
};
