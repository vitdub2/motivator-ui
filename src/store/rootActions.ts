import {createAction} from "redux-act";

export const clearAll = createAction("CLEAR_ALL");
