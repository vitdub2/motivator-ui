import {Handlers} from "redux-act";
import {clearAll} from "./rootActions";

export const rootHandlers = (defaultState: any): Handlers<any> => {
    return {
        [clearAll.getType()]: () => defaultState
    }
}
