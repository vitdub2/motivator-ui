import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { createHashHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import { composeWithDevTools } from "redux-devtools-extension";

import { rootReducer } from "./rootReducer";
import { rootSaga } from "./rootSaga";

const saga = createSagaMiddleware();

export const history = createHashHistory();

const state =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"
        ? composeWithDevTools(applyMiddleware(saga, routerMiddleware(history)))
        : applyMiddleware(saga, routerMiddleware(history));

export const store = createStore(rootReducer(history), state);

if (process.env.NODE_ENV && process.env.NODE_ENV === "development") {
    // @ts-ignore
    window.store = store;
}

saga.run(rootSaga);
