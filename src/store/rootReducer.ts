import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import {errorsReducer} from "../modules/errors/store/ducks";
import {loaderReducer} from "../modules/loader/store/ducks";
import {authReducer} from "../modules/auth/store/reducer";
import {modalsReducer} from "../modules/modals/store/reducer";
import {deviceReducer} from "../modules/devices/store/reducer";
import {taskReducer} from "../modules/tasks/store/reducer";

/*
Здесь указываются все reducer`ы

Reducer - часть стора, ответственная за определенную часть данных
*/
export const rootReducer = (history: any) =>
  combineReducers({
    router: connectRouter(history),
    errors: errorsReducer,
    loader: loaderReducer,
    auth: authReducer,
    modals: modalsReducer,
    devices: deviceReducer,
    tasks: taskReducer,
  });
