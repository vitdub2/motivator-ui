import {
    auth,
    confirmAccount,
    getUser,
    initApp,
    register,
    setConfirmEmail,
    setErrorMessage,
    setUser,
    signOut
} from "./actions";
import {all, call, put, takeLatest} from "redux-saga/effects";
import {hideLoader, showLoader} from "../../loader/store/ducks";
import {appError} from "../../errors/store/ducks";
import Lockr from 'lockr';
import {MOTIVATOR_DEVICE_ID, MOTIVATOR_FCM_TOKEN, MOTIVATOR_USER, MOTIVATOR_USER_TOKEN} from "./constants";
import {setAuthHeader} from "../../../utils/helpers";
import axios from "axios";
import {API_URLS} from "../../../store/constants";
import {getCurrentDevice} from "../../devices/store/actions";
import {clearAll} from "../../../store/rootActions";
import get from "lodash/get";

function* initAppSaga(action: ReturnType<typeof initApp>) {
    try {
        yield put(showLoader(action));

        const token = Lockr.get<string>(MOTIVATOR_USER_TOKEN);
        if (token) {
            setAuthHeader(token);
        }
        const deviceId = Lockr.get(MOTIVATOR_DEVICE_ID);
        if (token && !deviceId) {
            yield put(getCurrentDevice());
        }
    } catch (e) {
        console.log(e);
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

function* getUserSaga(action: ReturnType<typeof getUser>) {
    try {
        yield put(showLoader(action));

        const {
            data
        } = yield call(
            axios.get,
            API_URLS.AUTH.PROFILE
        );
        yield put(setUser(data));
        Lockr.set(MOTIVATOR_USER, data);
    } catch (e) {
        console.log(e);
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

function* authSaga(action: ReturnType<typeof auth>) {
    try {
        yield put(showLoader(action));

        const payload = action.payload;

        const {
            data: {
                token
            }
        } = yield call(
            axios.post,
            API_URLS.AUTH.LOGIN,
            {...payload}
        );

        Lockr.set(MOTIVATOR_USER_TOKEN, token);
        setAuthHeader(token);

        yield put(getUser());
        yield put(getCurrentDevice());
    } catch (e) {
        console.log(e);
        const status = get(e, "response.status");
        if (status === 401) {
            yield put(setErrorMessage(e.response.data.message || "Неверный пароль/логин"));
        } else {
            yield put(appError(e));
        }
    } finally {
        yield put(hideLoader(action));
    }
}

function* signOutSaga(action: ReturnType<typeof signOut>) {
    try {
        yield put(showLoader(action));

        const fcmToken = Lockr.get<string>(MOTIVATOR_FCM_TOKEN);

        Lockr.flush();

        if (fcmToken) {
            Lockr.set(MOTIVATOR_FCM_TOKEN, fcmToken);
        }

        yield put(clearAll());
    } catch (e) {
        console.log(e);
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

function* registerSaga(action: ReturnType<typeof register>) {
    try {
        yield put(showLoader(action));

        const info = action.payload;

        const {
            data: {
                email
            }
        } = yield call(
            axios.post,
            API_URLS.USERS,
            {...info}
        );

        yield put(setConfirmEmail(email));
    } catch (e) {
        console.log(e);
        const status = get(e, "response.status");
        if (status === 401) {
            yield put(setErrorMessage(e.response.data.message || "Неверный пароль/логин"));
        }
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

function* confirmAccountSaga(action: ReturnType<typeof confirmAccount>) {
    try {
        yield put(showLoader(action));

        const code = action.payload;

        const {
            data
        } = yield call(
            axios.get,
            `${API_URLS.AUTH.AUTH}/${code}/confirm`
        );

        const token = Lockr.get(MOTIVATOR_USER_TOKEN);
        if (!token) {
            Lockr.set(MOTIVATOR_USER_TOKEN, data.token);
            setAuthHeader(data.token);
        }

        yield put(getUser());

    } catch (e) {
        console.log(e);
        yield put(setErrorMessage(e.response.data.message));
    } finally {
        yield put(hideLoader(action));
    }
}

//Записываем все действия саги
export function* watchAuthActions() {
    yield all([
        //takeLatest(<действие>, <сага-обработчик_действия>)
        takeLatest(initApp, initAppSaga),
        takeLatest(getUser, getUserSaga),
        takeLatest(signOut, signOutSaga),
        takeLatest(auth, authSaga),
        takeLatest(register, registerSaga),
        takeLatest(confirmAccount, confirmAccountSaga),
    ]);
}
