import {IAuthState} from "./interfaces";
import {createReducer} from "redux-act";
import {rootHandlers} from "../../../store/rootHandlers";
import {clearConfirmEmail, clearErrorMessage, setConfirmEmail, setErrorMessage, setUser} from "./actions";
import {MOTIVATOR_USER} from "./constants";
import Lockr from 'lockr';

const defaultState: IAuthState = {
    user: Lockr.get(MOTIVATOR_USER)
}

export const authReducer = createReducer(rootHandlers({}), defaultState);

authReducer.on(setUser, (state, user) => ({...state, user}));

authReducer.on(setErrorMessage, (state, errorMessage) => ({...state, errorMessage}));

authReducer.on(clearErrorMessage, (state) => ({...state, errorMessage: undefined}));

authReducer.on(setConfirmEmail, (state, confirmEmail) => ({...state, confirmEmail}));

authReducer.on(clearConfirmEmail, (state) => ({...state, confirmEmail: undefined}));
