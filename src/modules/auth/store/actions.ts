import {createAction} from "redux-act";
import {IAuth, IRegister, IUser} from "./interfaces";

export const initApp = createAction(
    "INIT_APP"
);

export const getUser = createAction(
    "GET_USER"
);

export const setUser = createAction<IUser>(
    "SET_USER",
    (user: IUser) => user
);

export const signOut = createAction(
    "SIGN_OUT"
);

export const auth = createAction<IAuth>(
    "AUTH",
    (data: IAuth) => data
);

export const setErrorMessage = createAction<string>(
    "SET_ERROR_MESSAGE",
    (message: string) => message
);

export const clearErrorMessage = createAction(
    "CLEAR_ERROR_MESSAGE"
);

export const register = createAction<IRegister>(
    "REGISTER",
    (data: IRegister) => data
);

export const setConfirmEmail = createAction<string>(
    "SET_CONFIRM_EMAIL",
    (email: string) => email
);

export const clearConfirmEmail = createAction(
    "CLEAR_CONFIRM_EMAIL"
);

export const confirmAccount = createAction<string>(
    "CONFIRM_ACCOUNT",
    (confirmCode: string) => confirmCode
);
