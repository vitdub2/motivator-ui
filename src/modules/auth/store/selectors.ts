import {IAppState} from "../../../store/interfaces";
import {IAuthState} from "./interfaces";
import {createSelector} from "reselect";

//Получаем весь стейт приложения и оставляем стэйт модуля auth
const baseSelector = (state: IAppState): IAuthState => state.auth;

export const userSelector = createSelector(
    baseSelector,
    ({ user }) => user
);

export const isAuthenticatedSelector = createSelector(
    userSelector,
    (user) => !!user
);

export const errorMessageSelector = createSelector(
    baseSelector,
    ({ errorMessage }) => errorMessage
);

export const confirmEmailSelector = createSelector(
    baseSelector,
    ({ confirmEmail }) => confirmEmail
);
