export enum UserState {
    UNCONFIRMED = 'UNCONFIRMED',
    CONFIRMED = 'CONFIRMED',
}
