import {UserState} from "./enums";

export interface IAuthState {
    user?: IUser;
    confirmEmail?: string;
    errorMessage?: string;
}

export interface IUser {
    _id: string;
    username: string;
    email: string;
    state: UserState;
}

export interface IAuth {
    email: string;
    password: string;
}

export interface IRegister extends IAuth{
    username: string;
    passwordRepeat: string;
}
