import {FC, useEffect} from "react";
import {useHistory, useParams} from "react-router-dom";
import {URLS} from "../../../../store/constants";
import {useDispatch, useSelector} from "react-redux";
import {loaderSelector} from "../../../loader/store/ducks";
import {clearErrorMessage, confirmAccount, getUser} from "../../store/actions";
import {errorMessageSelector, userSelector} from "../../store/selectors";
import {IDevice} from "../../../devices/store/interfaces";
import {MOTIVATOR_FCM_TOKEN} from "../../store/constants";
import {getBrowserType, getDeviceType} from "../../../devices/store/helpers";
import {registerDevice} from "../../../devices/store/actions";
import {currentDeviceSelector} from "../../../devices/store/selectors";
import Lockr from 'lockr';
import {Rows} from "../../../../common/myControls/Grid/Grid";
import {Text} from "../../../../common/myControls/Text/Text";
import {TextTypes} from "../../../../common/myControls/Text/interfaces";
import {Button} from "../../../../common/myControls/Buttons/Button";

interface IProps {
    to?: string;
}

const RedirectButton: FC<IProps> = ({
    to= URLS.MAIN
}) => {
    const history = useHistory();

    const handleClick = () => {
        history.replace(to);
    }

    return (
        <Rows
            margin="20px 0 0 0"
            alignX="center"
        >
            <Button
                type="primary"
                click={handleClick}
                label="На главную"
            />
        </Rows>
    )
}

export const ConfirmPage: FC = () => {
    const {
        confirmCode
    } = useParams<any>();

    const history = useHistory();
    const dispatch = useDispatch();

    const isConfirming = useSelector(loaderSelector(confirmAccount));
    const isRegisteringDevice = useSelector(loaderSelector(registerDevice));
    const isGettingUser = useSelector(loaderSelector(getUser));

    const user = useSelector(userSelector);
    const errorMessage = useSelector(errorMessageSelector);

    const deviceId = useSelector(currentDeviceSelector);

    useEffect(() => {
        if (!confirmCode) {
            history.push(URLS.MAIN);
        }
    }, [confirmCode, history]);

    useEffect(() => {
        if (!user && !errorMessage && !isConfirming && !isGettingUser) {
            dispatch(confirmAccount(confirmCode));
        }
    }, [user, errorMessage, isConfirming, confirmCode, dispatch, isGettingUser]);

    useEffect(() => {
        if (user && !isRegisteringDevice && !errorMessage && !deviceId) {
            const deviceData: IDevice = {
                shouldBeNotified: true,
                token: Lockr.get(MOTIVATOR_FCM_TOKEN),
                browser: getBrowserType(),
                type: getDeviceType(),
                name: "TEST_DEVICE"
            }
            dispatch(registerDevice(deviceData));
        }
    }, [isRegisteringDevice, deviceId, user, dispatch, errorMessage]);

    useEffect(() => {
        dispatch(clearErrorMessage());
    }, [dispatch]);

    return (
        <>
            {errorMessage ? (
                <Rows>
                    <Text type={TextTypes.B20}>
                        {errorMessage}
                    </Text>
                    <RedirectButton/>
                </Rows>
            ) : (isConfirming || isGettingUser) ? (
                <>
                    <Text type={TextTypes.B20}>
                        Подтверждаем Ваш аккаунт...
                    </Text>
                </>
            ) : isRegisteringDevice ? (
                <>
                    <Text type={TextTypes.B20}>
                        Регистрируем Ваше устройство в системе...
                    </Text>
                </>
            ) : (
                <Rows>
                    <Text type={TextTypes.B20}>
                        {errorMessage}
                    </Text>
                    <RedirectButton
                        to={URLS.TASKS}
                    />
                </Rows>
            )}
        </>
    );
}
