import {FC, useEffect, useState} from "react";
import {useForm} from "react-hook-form";
import {REGISTER_FORM} from "../../../store/constants";
import {FormButton, FormInput} from "../../../../../common/myControls/FormInputs/FormInputs";
import {RULES} from "../../../../../common/myControls/Form/constants";
import {Rows} from "../../../../../common/myControls/Grid/Grid";
import {IForm} from "./AuthForm";

export const RegisterForm: FC<IForm> = ({
    submit,
    loading
}) => {
    const { handleSubmit, register, formState: {errors}, setError, clearErrors } = useForm({ reValidateMode: 'onSubmit' });

    const [password, setPassword] = useState<string>("");
    const [passwordRepeat, setPasswordRepeat] = useState<string>("");

    useEffect(() => {
        if (password && password !== passwordRepeat) {
            setError("passwordRepeat", {
                message: "Пароли не совпадают"
            })
        } else {
            clearErrors("passwordRepeat");
        }
    }, [clearErrors, setError, password, passwordRepeat]);

    return (
        <form onSubmit={handleSubmit(submit)} autoComplete="off" id={REGISTER_FORM}>
            <FormInput
                name="username"
                rules={RULES.REQUIRED.ANY}
                register={register}
                errors={errors}
                placeholder="Логин"
            />
            <FormInput
                name="email"
                rules={RULES.REQUIRED.EMAIL}
                register={register}
                errors={errors}
                placeholder="E-mail"
            />
            <FormInput
                name="password"
                rules={RULES.REQUIRED.ANY}
                register={register}
                errors={errors}
                type="password"
                onChange={setPassword}
                placeholder="Пароль"
            />
            <FormInput
                name="passwordRepeat"
                rules={RULES.REQUIRED.ANY}
                register={register}
                errors={errors}
                type="password"
                onChange={setPasswordRepeat}
                placeholder="Повторите пароль"
            />
            <Rows width="100%" alignX="center">
                <FormButton disabled={loading} label="Зарегистрироваться"/>
            </Rows>
        </form>
    );
}
