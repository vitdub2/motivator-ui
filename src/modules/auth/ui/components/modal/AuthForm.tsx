import {FC} from "react";
import {useForm} from "react-hook-form";
import {FormButton, FormInput} from "../../../../../common/myControls/FormInputs/FormInputs";
import {RULES} from "../../../../../common/myControls/Form/constants";
import {AUTH_FORM} from "../../../store/constants";
import {Rows} from "../../../../../common/myControls/Grid/Grid";

export interface IForm {
    submit: (data: any) => void;
    loading: boolean;
}

export const AuthForm: FC<IForm> = ({
   submit, loading
}) => {
    const { handleSubmit, register, formState: {errors} } = useForm({ reValidateMode: 'onChange' });
    return (
        <form onSubmit={handleSubmit(submit)} autoComplete="off" id={AUTH_FORM}>
            <FormInput
                name="email"
                rules={RULES.REQUIRED.ANY}
                register={register}
                errors={errors}
                placeholder="E-mail / Логин"
            />
            <FormInput
                name="password"
                rules={RULES.REQUIRED.ANY}
                register={register}
                errors={errors}
                type="password"
                placeholder="Пароль"
            />
            <Rows width="100%" alignX="center">
                <FormButton id="modal_sign_in_btn" disabled={loading} label="Войти"/>
            </Rows>
        </form>
    );
}
