import React from "react";
import { useDispatch, useSelector } from "react-redux";
import values from "lodash/values";
import last from "lodash/last";
import initial from "lodash/initial";
import slice from "lodash/slice";
import findIndex from "lodash/findIndex";

import { Block } from "../../../../common/myControls/Grid/Grid";

import { setModals } from "../../store/actions";
import { modalsSelector, modalsTotalSelector } from "../../store/selectors";
import { Modal } from "../../store/interfaces";
import {MODAL_TYPE} from "../../../../store/constants";
import {AuthModal} from "../components/AuthModal";
import {RegisterModal} from "../components/RegisterModal";
import {ConfirmEmailModal} from "../components/ConfirmEmailModal";
import {TasksWarningModal} from "../components/TasksWarningModal";
import {TaskCreateModal} from "../components/TaskCreateModal";
import {TaskEditModal} from "../components/TaskEditModal";

export const Modals = () => {
  const dispatch = useDispatch();

  const modals = useSelector(modalsSelector);
  const modalsTotal = useSelector(modalsTotalSelector);

  const newModal = last(modals);
  const modalsWithoutLast = initial(modals);

  const composeModals = (modal: Modal, newModal: Modal) => {
    const modalValue = values(modal.modalProps);
    const newModalValue = values(newModal.modalProps);

    return (
      modal.modalType === newModal.modalType &&
      modalValue[0] === newModalValue[0]
    );
  };

  if (modalsTotal > 1 && newModal) {
    const index = findIndex(modalsWithoutLast, (modal: Modal) =>
      composeModals(modal, newModal)
    );

    if (~index) {
      const takeModals = slice(modals, 0, index + 1);
      dispatch(setModals(takeModals));
    }
  }

  return (
    <>
      {modals.map(({ modalType, modalProps, modalId }) => (
        <Block key={modalId}>
          {modalType === MODAL_TYPE.AUTH && (
              <AuthModal/>
          )}
          {modalType === MODAL_TYPE.REGISTER && (
              <RegisterModal/>
          )}
          {modalType === MODAL_TYPE.CONFIRM_EMAIL && (
              <ConfirmEmailModal/>
          )}
          {modalType === MODAL_TYPE.TASKS_WARNING && (
              <TasksWarningModal/>
          )}
          {modalType === MODAL_TYPE.TASK_CREATE && (
              <TaskCreateModal/>
          )}
          {modalType === MODAL_TYPE.TASK_EDIT && (
              <TaskEditModal modalProps={modalProps}/>
          )}
        </Block>
      ))}
    </>
  );
};
