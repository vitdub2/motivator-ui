import React, {FC, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import delay from "lodash/delay";
import {hideModal} from "../../store/actions";
import {TIMEOUT} from "../../../../common/myControls/Modal/constants";
import {ModalComp} from "../../../../common/myControls/Modal/ModalComp";
import {Block, Rows} from "../../../../common/myControls/Grid/Grid";
import {Text} from "../../../../common/myControls/Text/Text";
import {TextTypes} from "../../../../common/myControls/Text/interfaces";
import {TaskForm} from "../../../tasks/ui/components/modal/TaskForm";
import {loaderSelector} from "../../../loader/store/ducks";
import {createTask} from "../../../tasks/store/actions";
import map from "lodash/map";
import {convertTimeStrToTimezone} from "../../../tasks/store/helpers";

export const TaskCreateModal: FC = () => {
    const dispatch = useDispatch();
    const [toggle, setToggle] = useState<boolean>(false);

    const handleCloseModal = () => {
        setToggle(false);
        delay(() => dispatch(hideModal()), TIMEOUT.MODAL.EXIT.MS);
    }

    const isSending = useSelector(loaderSelector(createTask));

    const handleSubmit = (data: any) => {
        dispatch(createTask({
            ...data,
            notificationTimes: map(data.notificationTimes, convertTimeStrToTimezone),
        }));
    }

    return (
        <ModalComp
            toggle={toggle}
            setToggle={setToggle}
            closeModal={handleCloseModal}
            content={{ borderRadius: "top" }}
        >
            <Rows
                alignX="center"
                padding="10px"
            >
                <Block margin="0 0 20px 0">
                    <Text type={TextTypes.B20}>
                        Создание активности
                    </Text>
                </Block>
                <TaskForm submit={handleSubmit} loading={isSending}/>
            </Rows>
        </ModalComp>
    );
}
