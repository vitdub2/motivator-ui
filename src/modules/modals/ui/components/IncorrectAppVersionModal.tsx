import React, { FC, useState } from "react";
import { useDispatch } from "react-redux";
import delay from "lodash/delay";

import { ModalComp } from "../../../../common/myControls/Modal/ModalComp";
import { Button } from "../../../../common/myControls/Buttons/Button";
import { Block } from "../../../../common/myControls/Grid/Grid";
import { Text } from "../../../../common/myControls/Text/Text";

import { hideModal } from "../../store/actions";
import { TIMEOUT } from "../../../../common/myControls/Modal/constants";
import { TextTypes } from "../../../../common/myControls/Text/interfaces";

export const IncorrectAppVersionModal: FC = () => {
  const dispatch = useDispatch();
  const [toggle, setToggle] = useState<boolean>(false);

  const handleCloseModal = () => {
    setToggle(false);
    delay(() => dispatch(hideModal()), TIMEOUT.MODAL.EXIT.MS);
  };

  const reload = () => {
    window.location.reload(true);
  };

  return (
    <>
      <ModalComp
        toggle={toggle}
        setToggle={setToggle}
        closeModal={handleCloseModal}
        content={{ borderRadius: "top" }}
      >

        <Block margin="default">
          <Text type={TextTypes.R16G}>
            Текущая версия приложения устарела, для обновления необходимо
            перезагрузить страницу
          </Text>
        </Block>

        <Block margin="default">
          <Button type="primary" label="перезагрузка" click={reload} />
        </Block>
      </ModalComp>
    </>
  );
};
