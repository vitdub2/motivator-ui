import {FC, useEffect, useState} from "react";
import {ModalComp} from "../../../../common/myControls/Modal/ModalComp";
import {Block, Rows} from "../../../../common/myControls/Grid/Grid";
import {Text} from "../../../../common/myControls/Text/Text";
import {TextTypes} from "../../../../common/myControls/Text/interfaces";
import {useDispatch, useSelector} from "react-redux";
import delay from "lodash/delay";
import {hideModal} from "../../store/actions";
import {TIMEOUT} from "../../../../common/myControls/Modal/constants";
import {confirmEmailSelector} from "../../../auth/store/selectors";
import {clearConfirmEmail} from "../../../auth/store/actions";

export const ConfirmEmailModal: FC = () => {
    const dispatch = useDispatch();
    const [toggle, setToggle] = useState<boolean>(false);

    const handleCloseModal = () => {
        setToggle(false);
        delay(() => dispatch(hideModal()), TIMEOUT.MODAL.EXIT.MS);
    };

    const confirmEmail = useSelector(confirmEmailSelector);

    useEffect(() => {
        return (() => {
            dispatch(clearConfirmEmail());
        })
    }, [dispatch])

    return (
        <>
            <ModalComp
                toggle={toggle}
                setToggle={setToggle}
                closeModal={handleCloseModal}
                content={{ borderRadius: "top" }}
            >
                <Rows height="100%" padding="default" alignX="center" alignY="center">
                    <Block margin="0 0 20px 0">
                        <Text type={TextTypes.B20}>
                            Подтверждение
                        </Text>
                    </Block>
                    <Text type={TextTypes.R16}>
                        Мы отправили подтверждение на {confirmEmail}
                    </Text>
                </Rows>
            </ModalComp>
        </>
    );
}
