import {useDispatch, useSelector} from "react-redux";
import {clearErrorMessage, register} from "../../../auth/store/actions";
import {confirmEmailSelector, errorMessageSelector} from "../../../auth/store/selectors";
import delay from "lodash/delay";
import {addModal, hideModal} from "../../store/actions";
import {TIMEOUT} from "../../../../common/myControls/Modal/constants";
import {ModalComp} from "../../../../common/myControls/Modal/ModalComp";
import {Block, Rows} from "../../../../common/myControls/Grid/Grid";
import {Text} from "../../../../common/myControls/Text/Text";
import {TextTypes} from "../../../../common/myControls/Text/interfaces";
import {COLOR} from "../../../../common/consts/styles";
import {FC, useCallback, useEffect, useState} from "react";
import {IRegister} from "../../../auth/store/interfaces";
import {loaderSelector} from "../../../loader/store/ducks";
import {RegisterForm} from "../../../auth/ui/components/modal/RegisterForm";
import {MODAL_TYPE} from "../../../../store/constants";

export const RegisterModal: FC = () => {
    const dispatch = useDispatch();
    const [toggle, setToggle] = useState<boolean>(false);

    const errorMessage = useSelector(errorMessageSelector);

    const isRegistering = useSelector(loaderSelector(register));

    const confirmEmail = useSelector(confirmEmailSelector);

    const handleCloseModal = useCallback(
        () => {
            if (!isRegistering) {
                setToggle(false);
                delay(() => dispatch(hideModal()), TIMEOUT.MODAL.EXIT.MS);
            }
        }, [isRegistering, dispatch]
    );

    const onSubmit = (data: IRegister) => {
        const {passwordRepeat, ...info} = data;
        dispatch(register(info));
    };

    useEffect(() => {
        if (!isRegistering && confirmEmail) {
            handleCloseModal();
            delay(() => dispatch(addModal(MODAL_TYPE.CONFIRM_EMAIL)), TIMEOUT.MODAL.EXIT.MS);
        }
    }, [isRegistering, confirmEmail, dispatch, handleCloseModal])

    useEffect(() => {
        return () => {
            dispatch(clearErrorMessage());
        }
    }, [dispatch]);

    return (
        <>
            <ModalComp
                toggle={toggle}
                setToggle={setToggle}
                closeModal={handleCloseModal}
                content={{ borderRadius: "top" }}
            >
                <Rows height="100%" padding="default" alignX="center" alignY="center">
                    <Block margin="0 0 20px 0">
                        <Text type={TextTypes.B20}>
                            Регистрация
                        </Text>
                    </Block>
                    <RegisterForm loading={isRegistering} submit={onSubmit}/>
                    {errorMessage && (
                        <Block margin="10px 0 0 0">
                            <Text type={TextTypes.B16} color={COLOR.TOMATO}>
                                {errorMessage}
                            </Text>
                        </Block>
                    )}
                </Rows>
            </ModalComp>
        </>
    );
}
