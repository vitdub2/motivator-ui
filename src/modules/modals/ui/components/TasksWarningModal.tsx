import React, {FC, useState} from "react";
import {useDispatch} from "react-redux";
import delay from "lodash/delay";
import {hideModal} from "../../store/actions";
import {TIMEOUT} from "../../../../common/myControls/Modal/constants";
import {ModalComp} from "../../../../common/myControls/Modal/ModalComp";
import {Block, Rows} from "../../../../common/myControls/Grid/Grid";
import {Text} from "../../../../common/myControls/Text/Text";
import {TextTypes} from "../../../../common/myControls/Text/interfaces";
import {MOTIVATOR_FCM_TIMESTAMP} from "../../../auth/store/constants";
import Lockr from 'lockr';
import {formatDate} from "../../../tasks/store/helpers";

export const TasksWarningModal: FC = () => {
    const dispatch = useDispatch();
    const [toggle, setToggle] = useState<boolean>(false);

    const handleCloseModal = () => {
        setToggle(false);
        delay(() => dispatch(hideModal()), TIMEOUT.MODAL.EXIT.MS);
    }

    const createdTimestamp = Lockr.get<number>(MOTIVATOR_FCM_TIMESTAMP);
    const secureDate = new Date(createdTimestamp + 1000 * 60 * 30);

    return (
        <>
            <ModalComp
                toggle={toggle}
                setToggle={setToggle}
                closeModal={handleCloseModal}
                content={{ borderRadius: "top" }}
            >
                <Rows
                    width="100%"
                    alignX="center"
                    padding="10px"
                >
                    <Block margin="0 0 20px 0">
                        <Text type="B30">
                            Предупреждение
                        </Text>
                    </Block>
                    <Rows>
                        <Block margin="0 0 20px 0">
                            <Text type={TextTypes.R16}>
                                Настоятельно не рекомендуем создавать активности со временем оповещения,
                                меньшим чем полчаса с момента регистрации (до {`${formatDate(secureDate)}`})
                            </Text>
                        </Block>
                        <Text type={TextTypes.R16}>
                            До этого времени оповещения на данное сетевое устройство могут не придти
                        </Text>
                    </Rows>
                </Rows>
            </ModalComp>
        </>
    );
}
