import React, {FC, useCallback, useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import delay from "lodash/delay";
import {hideModal} from "../../store/actions";
import {TIMEOUT} from "../../../../common/myControls/Modal/constants";
import {ModalComp} from "../../../../common/myControls/Modal/ModalComp";
import {Block, Rows} from "../../../../common/myControls/Grid/Grid";
import {Text} from "../../../../common/myControls/Text/Text";
import {TextTypes} from "../../../../common/myControls/Text/interfaces";
import {AuthForm} from "../../../auth/ui/components/modal/AuthForm";
import {IAuth} from "../../../auth/store/interfaces";
import {auth, clearErrorMessage, getUser} from "../../../auth/store/actions";
import {loaderSelector} from "../../../loader/store/ducks";
import {errorMessageSelector, isAuthenticatedSelector} from "../../../auth/store/selectors";
import {getCurrentDevice} from "../../../devices/store/actions";
import {COLOR} from "../../../../common/consts/styles";

export const AuthModal: FC = () => {
    const dispatch = useDispatch();
    const [toggle, setToggle] = useState<boolean>(false);

    const isAuthorizing = useSelector(loaderSelector(auth));
    const isGettingUser = useSelector(loaderSelector(getUser));
    const isGettingDevice = useSelector(loaderSelector(getCurrentDevice));

    const isAuthenticating = isAuthorizing || isGettingDevice || isGettingUser;

    const isAuthenticated = useSelector(isAuthenticatedSelector);

    const errorMessage = useSelector(errorMessageSelector);

    const handleCloseModal = useCallback(
        () => {
            if (!isAuthorizing) {
                setToggle(false);
                delay(() => dispatch(hideModal()), TIMEOUT.MODAL.EXIT.MS);
            }
        }, [isAuthorizing, dispatch]
    );

    const onSubmit = (data: IAuth) => {
        dispatch(auth(data));
        dispatch(clearErrorMessage());
    };

    useEffect(() => {
        return () => {
            dispatch(clearErrorMessage());
        }
    }, [dispatch]);

    useEffect(() => {
        if (isAuthenticated) {
            handleCloseModal();
        }
    },[isAuthenticated, handleCloseModal]);

    return (
        <>
            <ModalComp
                toggle={toggle}
                setToggle={setToggle}
                closeModal={handleCloseModal}
                content={{ borderRadius: "top" }}
            >
                <Rows height="100%" padding="default" alignX="center" alignY="center">
                    <Block margin="0 0 20px 0">
                        <Text type={TextTypes.B20}>
                            Авторизация
                        </Text>
                    </Block>
                    <AuthForm loading={isAuthenticating} submit={onSubmit}/>
                    {errorMessage && (
                        <Block margin="10px 0 0 0">
                            <Text type={TextTypes.B16} id="auth_error_message" color={COLOR.TOMATO}>
                                {errorMessage}
                            </Text>
                        </Block>
                    )}
                </Rows>
            </ModalComp>
        </>
    );
}
