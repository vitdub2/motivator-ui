export type Modal = {
  modalType: string;
  modalProps: {
    [key: string]: any;
  };
  [key: string]: any;
};

export interface IModalsState {
  modals: Modal[];
}
