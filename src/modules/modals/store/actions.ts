import { createAction } from "redux-act";

import { Modal } from "./interfaces";

export const addModal: any = createAction(
  "ADD_MODAL",
  (modalType: string, modalProps: any): Modal => ({
    modalType,
    modalProps
  })
);

export const setModals: any = createAction(
  "SET_MODALS",
  (modals: Modal[]) => modals
);

export const hideModal: any = createAction("HIDE_MODAL");

export const hideAllModal: any = createAction("HIDE_ALL_MODAL");
