import { createSelector } from "reselect";
import size from "lodash/size";

import { IModalsState } from "./interfaces";

export const modalsStateSelector = (state: any): IModalsState => state.modals;

export const modalsSelector = createSelector(
  modalsStateSelector,
  ({ modals }: IModalsState) => modals
);

export const modalsTotalSelector = createSelector(modalsSelector, modals =>
  size(modals)
);
