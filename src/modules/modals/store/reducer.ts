import { createReducer } from "redux-act";
import concat from "lodash/concat";
import assign from "lodash/assign";
import initial from "lodash/initial";
import { v4 as uuidv4 } from "uuid";

import { addModal, hideAllModal, hideModal, setModals } from "./actions";
import { IModalsState, Modal } from "./interfaces";
import {rootHandlers} from "../../../store/rootHandlers";

const defaultState: IModalsState = {
  modals: []
};

export const modalsReducer = createReducer(
  {
    ...rootHandlers(defaultState),
    [addModal]: (state: IModalsState, modal: Modal) => {
      assign(modal, { modalId: uuidv4() });
      return {
        ...state,
        modals: concat(state.modals, modal)
      };
    },

    [setModals]: (state: IModalsState, modals: Modal[]) => ({
      ...state,
      modals
    }),

    [hideModal]: (state: IModalsState) => {
      const stateWithoutLastModal = initial(state.modals);
      return { ...state, modals: stateWithoutLastModal };
    },

    [hideAllModal]: () => defaultState
  },
  defaultState
);
