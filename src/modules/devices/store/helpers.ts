import {BrowserTypes, DeviceTypes} from "./enums";

export const getBrowserType = (): BrowserTypes => {
    return BrowserTypes.CHROME;
}

export const getDeviceType = (): DeviceTypes => {
    return DeviceTypes.DESKTOP;
}
