export enum DeviceTypes {
    MOBILE = 'MOBILE',
    DESKTOP = 'DESKTOP',
}

export enum BrowserTypes {
    CHROME = "CHROME"
}
