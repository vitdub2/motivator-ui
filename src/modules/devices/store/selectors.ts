//Получаем весь стейт приложения и оставляем стэйт модуля auth
import {IAppState} from "../../../store/interfaces";
import {IDeviceState} from "./interfaces";
import {createSelector} from "reselect";

const baseSelector = (state: IAppState): IDeviceState => state.devices;

export const currentDeviceSelector = createSelector(
    baseSelector,
    ({ currentDevice }) => currentDevice
);

export const devicesSelector = createSelector(
    baseSelector,
    ({ devices }) => devices
);
