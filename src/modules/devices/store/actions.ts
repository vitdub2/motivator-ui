import {createAction} from "redux-act";
import {IDevice, IDeviceTokenDto} from "./interfaces";

export const getCurrentDevice = createAction(
    "GET_CURRENT_DEVICE"
);

export const setCurrentDevice = createAction<string>(
    "SET_CURRENT_DEVICE",
    (deviceId: string) => deviceId
);

export const getDevices = createAction(
    "GET_DEVICES"
);

export const setDevices = createAction<IDevice[]>(
    "SET_DEVICES",
    (devices: IDevice[]) => devices
);

export const registerDevice = createAction<IDevice>(
    "REGISTER_DEVICE",
    (device: IDevice) => device
);

export const updateToken = createAction<IDeviceTokenDto>(
    "UPDATE_TOKEN",
    (dto: IDeviceTokenDto) => dto
);
