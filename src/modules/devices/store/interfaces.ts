import {BrowserTypes, DeviceTypes} from "./enums";

export interface IDevice {
    shouldBeNotified: boolean;
    token: string;
    name?: string;
    _id?: string;
    type: DeviceTypes;
    browser: BrowserTypes;
    userId?: string;
}

export interface IDeviceState {
    devices: IDevice[],
    currentDevice?: string;
}

export interface IDeviceTokenDto {
    token: string;
    deviceId: string;
}
