import {IDeviceState} from "./interfaces";
import {createReducer} from "redux-act";
import {rootHandlers} from "../../../store/rootHandlers";
import {setCurrentDevice, setDevices} from "./actions";
import {MOTIVATOR_DEVICE_ID} from "../../auth/store/constants";
import Lockr from 'lockr';

const defaultValues: IDeviceState = {
    devices: [],
    currentDevice: Lockr.get<string>(MOTIVATOR_DEVICE_ID)
};

export const deviceReducer = createReducer(rootHandlers(defaultValues), defaultValues);

deviceReducer.on(setCurrentDevice, (state, currentDevice) => ({...state, currentDevice}));

deviceReducer.on(setDevices, (state, devices) => ({...state, devices}));
