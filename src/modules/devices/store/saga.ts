import {call, put, takeLatest, all} from "redux-saga/effects";
import {hideLoader, showLoader} from "../../loader/store/ducks";
import axios from "axios";
import {API_URLS} from "../../../store/constants";
import {appError} from "../../errors/store/ducks";
import {getCurrentDevice, getDevices, registerDevice, setCurrentDevice, setDevices, updateToken} from "./actions";
import first from 'lodash/first';
import {IDevice} from "./interfaces";
import {MOTIVATOR_DEVICE_ID, MOTIVATOR_FCM_TOKEN} from "../../auth/store/constants";
import Lockr from 'lockr';
import {setErrorMessage} from "../../auth/store/actions";

function* getDevicesSaga(action: ReturnType<typeof getDevices>) {
    try {
        yield put(showLoader(action));
        const {
            data
        } = yield call(
            axios.get,
            API_URLS.DEVICES
        );
        yield put(setDevices(data));
    } catch (e) {
        console.log(e);
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

function* getCurrentDeviceSaga(action: ReturnType<typeof getCurrentDevice>) {
    try {
        yield put(showLoader(action));
        const {
            data
        } = yield call(
            axios.get,
            API_URLS.DEVICES
        );
        // @ts-ignore
        const device: IDevice = first(data);
        const id = device._id || "";
        Lockr.set(MOTIVATOR_DEVICE_ID, id);
        yield put(setCurrentDevice(id));

        const token = Lockr.get(MOTIVATOR_FCM_TOKEN);
        yield put(updateToken({
            deviceId: id,
            token
        }));
    } catch (e) {
        console.log(e);
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

function* registerDeviceSaga(action: ReturnType<typeof registerDevice>) {
    try {
        yield put(showLoader(action));
        const deviceData = action.payload;
        const {
            data
        } = yield call(
            axios.post,
            API_URLS.DEVICES,
            {...deviceData}
        );
        const id = data._id;
        yield put(setCurrentDevice(id));
        Lockr.set(MOTIVATOR_DEVICE_ID, id);
    } catch (e) {
        console.log(e);
        yield put(setErrorMessage(e.response.data.message));
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

function* updateTokenSaga (action: ReturnType<typeof updateToken>) {
    try {
        yield put(showLoader(action));

        const {deviceId, token} = action.payload;

        yield call(
            axios.put,
            `${API_URLS.DEVICES}/${deviceId}`,
            {token}
        )
    } catch (e) {
        console.log(e);
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

export function* watchDevicesActions() {
    yield all([
        takeLatest(getDevices, getDevicesSaga),
        takeLatest(getCurrentDevice, getCurrentDeviceSaga),
        takeLatest(registerDevice, registerDeviceSaga),
        takeLatest(updateToken, updateTokenSaga),
    ])
}
