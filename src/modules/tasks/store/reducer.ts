import {ITask, ITaskState} from "./interfaces";
import {createReducer} from "redux-act";
import {rootHandlers} from "../../../store/rootHandlers";
import {addTask, clearTasks, putTask, removeTask, setTasks} from "./actions";
import unionBy from "lodash/unionBy";
import pullAll from "lodash/pullAll";
import filter from "lodash/filter";

const defaultState: ITaskState = {};

export const taskReducer = createReducer(rootHandlers(defaultState), defaultState);

taskReducer.on(setTasks, (state, tasks) => ({...state, tasks}));

taskReducer.on(clearTasks, (state) => ({...state, tasks: undefined}));

taskReducer.on(addTask, (state, task) => ({
    ...state,
    tasks: [...unionBy(state.tasks, [task], '_id')]
}));

taskReducer.on(removeTask, (state, id) => ({
    ...state,
    tasks: [...pullAll(state.tasks, filter(state.tasks, (task: ITask) => task._id === id))]
}));

taskReducer.on(putTask, (state, task) => ({
    ...state,
    tasks: [...filter(state.tasks, (t: ITask) => t._id !== task._id), task]
}))
