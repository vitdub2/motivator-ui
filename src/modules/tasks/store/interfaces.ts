export interface TaskDto {
    notificationTimes: string[];
    name: string;
    desc: string;
}

export interface ITask extends TaskDto {
    _id: string;
}

export interface ITaskState {
    tasks?: ITask[];
}
