import {createAction} from "redux-act";
import {ITask, TaskDto} from "./interfaces";

export const getTasks = createAction(
    "GET_TASKS"
);

export const clearTasks = createAction(
    "CLEAR_TASKS"
);

export const setTasks = createAction<ITask[]>(
    "SET_TASKS",
    (tasks: ITask[]) => tasks
);

export const removeTask = createAction<string>(
    "REMOVE_TASK",
    (id: string) => id
);

export const createTask = createAction<TaskDto>(
    "CREATE_TASK",
    (task: TaskDto) => task
);

export const addTask = createAction<ITask>(
    "ADD_TASK",
    (task: ITask) => task
);

export const putTask = createAction<ITask>(
    "PUT_TASK",
    (task: ITask) => task
);
