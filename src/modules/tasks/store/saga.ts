import {addTask, createTask, getTasks, putTask, removeTask, setTasks} from "./actions";
import {all, call, put, takeLatest} from "redux-saga/effects";
import {hideLoader, showLoader} from "../../loader/store/ducks";
import {appError} from "../../errors/store/ducks";
import axios from "axios";
import {API_URLS} from "../../../store/constants";
import {setErrorMessage} from "../../auth/store/actions";

function* getTasksSaga(action: ReturnType<typeof getTasks>) {
    try {
        yield put(showLoader(action));

        const {
            data
        } = yield call(
            axios.get,
            API_URLS.TASKS
        );

        yield put(setTasks(data));
    } catch (e) {
        console.log(e);
        yield put(setErrorMessage('Ошибка сети'));
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

function* createTaskSaga(action: ReturnType<typeof createTask>) {
    try {
        yield put(showLoader(action));

        const dto = action.payload;

        const {
            data
        } = yield call(
            axios.post,
            API_URLS.TASKS,
            {...dto}
        );
        yield put(addTask(data));
    } catch (e) {
        console.log(e);
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

function* removeTaskSaga (action: ReturnType<typeof removeTask>) {
    try {
        yield put(showLoader(action));

        const id = action.payload;
        yield call(
            axios.delete,
            `${API_URLS.TASKS}/${id}`,
        );
    } catch (e) {
        console.log(e);
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

function* putTaskSaga (action: ReturnType<typeof putTask>) {
    try {
        yield put(showLoader(action));

        const task = action.payload;
        yield call(
            axios.put,
            `${API_URLS.TASKS}/${task._id}`,
            {...task}
        );
    } catch (e) {
        console.log(e);
        yield put(appError(e));
    } finally {
        yield put(hideLoader(action));
    }
}

export function* watchTasksAction() {
    yield all([
        takeLatest(getTasks, getTasksSaga),
        takeLatest(createTask, createTaskSaga),
        takeLatest(removeTask, removeTaskSaga),
        takeLatest(putTask, putTaskSaga),
    ]);
}
