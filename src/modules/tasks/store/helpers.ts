import {TaskDto} from "./interfaces";
import map from "lodash/map";

const setHoursAndMinutes = (dateStr: string): Date => {
    const parts = dateStr.split(':');

    const hours = +parts[0];
    const minutes = +parts[1];

    const date = new Date();
    date.setHours(hours);
    date.setMinutes(minutes);

    return date;
}

export const getDateFromStr = (dateStr: string): Date => {
    const date = setHoursAndMinutes(dateStr);

    date.setTime(date.getTime() - (date.getTimezoneOffset() + 180) * 60 * 1000);

    return date;
}

export const convertTimeStrToTimezone = (dateStr: string): string => {
    const date = setHoursAndMinutes(dateStr);

    date.setTime(date.getTime() + (date.getTimezoneOffset() + 180) * 60 * 1000);

    return formatDate(date);
}

export const formatDate = (date: Date | undefined): string => {
    if (!date) return '';
    return `${date.getHours().toString().padStart(2, "0")}:${date.getMinutes().toString().padStart(2, "0")}`;
}

export const parseFormDataToTaskDto = (data: any): TaskDto => {
    const {name, desc} = data;
    const keys = Object.keys(data);
    const timesCount = keys.length - 2;
    const rawNotificationTimes: Array<string> = [];
    for (let i = 0; i < timesCount; i++) {
        const time = data[`notificationTimes.${i}`];
        if (time) {
            rawNotificationTimes.push(time);
        }
    }

    const notificationTimes = map(rawNotificationTimes, convertTimeStrToTimezone);

    return {
        name,
        desc,
        notificationTimes
    }
}
