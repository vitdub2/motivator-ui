import {IAppState} from "../../../store/interfaces";
import {ITaskState} from "./interfaces";
import {createSelector} from "reselect";

const baseSelector = (state: IAppState): ITaskState => state.tasks;

export const tasksSelector = createSelector(
    baseSelector,
    ({ tasks }) => tasks
);
