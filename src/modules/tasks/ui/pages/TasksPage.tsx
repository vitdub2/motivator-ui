import {FC, useEffect} from "react";
import {Columns, Rows} from "../../../../common/myControls/Grid/Grid";
import {Text} from "../../../../common/myControls/Text/Text";
import {TextTypes} from "../../../../common/myControls/Text/interfaces";
import {useDispatch, useSelector} from "react-redux";
import {tasksSelector} from "../../store/selectors";
import {loaderSelector} from "../../../loader/store/ducks";
import {clearTasks, getTasks} from "../../store/actions";
import map from 'lodash/map';
import isEmpty from 'lodash/isEmpty';
import {ITask} from "../../store/interfaces";
import {TaskItem} from "../components/TaskItem";
import {Button} from "../../../../common/myControls/Buttons/Button";
import Lockr from 'lockr';
import {MOTIVATOR_FCM_TIMESTAMP} from "../../../auth/store/constants";
import styled from "styled-components";
import {ReactIcon} from "../../../../common/myControls/ReactIcon/ReactIcon";
import {COLOR} from "../../../../common/consts/styles";
import {Responsiveness} from "../../../../common/myControls/constants";
import {addModal} from "../../../modals/store/actions";
import {MODAL_TYPE} from "../../../../store/constants";
import {errorMessageSelector} from "../../../auth/store/selectors";

const WarningWrapper = styled(Rows)`
  margin-left: 10px;
  
  ${Responsiveness}
`;

const TasksWrapper = styled(Rows)`
  max-height: 60vh;
  overflow: auto;
  width: 100%;
`;

export const TasksPage: FC = () => {
    const tasks = useSelector(tasksSelector);
    const isGettingTasks = useSelector(loaderSelector(getTasks));

    const dispatch = useDispatch();
    const errorMessage = useSelector(errorMessageSelector);

    const handleCreateClick = () => {
        dispatch(addModal(MODAL_TYPE.TASK_CREATE));
    }

    const handleWarningClick = () => {
        dispatch(addModal(MODAL_TYPE.TASKS_WARNING));
    }

    useEffect(() => {
        if (!isGettingTasks && !tasks && !errorMessage) {
            dispatch(getTasks());
        }
    }, [isGettingTasks, tasks, dispatch, errorMessage]);

    useEffect(() => {
        return () => {
            dispatch(clearTasks());
        }
    }, [dispatch]);

    const tokenTimestamp = Lockr.get<number>(MOTIVATOR_FCM_TIMESTAMP) || new Date().getTime();
    const currentTimestamp = new Date().getTime();
    const diff = currentTimestamp - tokenTimestamp;

    const shouldDisplayWarning = diff <= 1000 * 60 * 30;

    return (
        <Rows
            alignX="center"
            width="50%"
        >
          <Columns
              alignX="center"
              alignY="center"
              margin="0 0 20px 0"
          >
              <Text type={TextTypes.B20}>
                  Мои активности
              </Text>
              {shouldDisplayWarning && diff !== 0 && (
                  <WarningWrapper
                      alignY="center"
                      onClick={handleWarningClick}
                  >
                      <ReactIcon
                          name="IoIosWarning"
                          size="21px"
                          color={COLOR.ORANGE}
                      />
                  </WarningWrapper>
              )}
          </Columns>
          <Rows
              alignX="center"
              margin="0 0 20px 0"
              width="100%"
          >
              <>
                  {isEmpty(tasks) ?
                      errorMessage ? (
                          <>
                              {errorMessage}
                          </>
                      ) : isGettingTasks ? (
                          <>
                              Загрузка...
                          </>
                      ) : (
                          <>
                              У вас нет активностей. Самое время их создать!
                          </>
                  ) : (
                      <TasksWrapper>
                          {map(tasks, (task: ITask) => {
                              return (
                                  <TaskItem
                                      task={task}
                                      key={task._id}
                                  />
                              );
                          })}
                      </TasksWrapper>
                  )}
              </>
          </Rows>
          <Button
              type="primary"
              label="Создать активность"
              click={handleCreateClick}
              disabled={!!errorMessage}
          />
        </Rows>
    );
}
