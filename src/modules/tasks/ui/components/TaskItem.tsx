import {ITask} from "../../store/interfaces";
import {FC, useState} from "react";
import {Block, Columns, Rows} from "../../../../common/myControls/Grid/Grid";
import {CSSTransition} from "react-transition-group";
import styled from "styled-components";
import {TIMEOUT} from "../../../../common/myControls/Modal/constants";
import {BORDER, COLOR} from "../../../../common/consts/styles";
import {Responsiveness} from "../../../../common/myControls/constants";
import {Z_INDEX} from "../../../../common/consts/constants";
import {Text} from "../../../../common/myControls/Text/Text";
import {TextTypes} from "../../../../common/myControls/Text/interfaces";
import {ReactIcon} from "../../../../common/myControls/ReactIcon/ReactIcon";
import sortBy from "lodash/sortBy";
import map from "lodash/map";
import first from "lodash/first";
import filter from "lodash/filter";
import {formatDate, getDateFromStr} from "../../store/helpers";
import {removeTask} from "../../store/actions";
import {useDispatch} from "react-redux";
import last from "lodash/last";
import {addModal} from "../../../modals/store/actions";
import {MODAL_TYPE} from "../../../../store/constants";

interface IProps {
    task: ITask,
}

const TaskCard = styled(Rows)`
  height: 0;
  z-index: ${Z_INDEX.TASK_DESC};
  border: ${BORDER.DEFAULT};
  border-top: none;
  padding: 10px;
  padding-top: 20px;
  border-radius: 10px;
  border-top-right-radius: 0;
  border-top-left-radius: 0;
  
  &.taskCard-enter {
    height: 0;
    transform: translateY(-40px);
  }

  &.taskCard-enter-active {
    height: 100px;
    transform: translateY(-10px);
    transition: 0.5s;
  }

  &.taskCard-enter-done {
    height: 100px;
    transform: translateY(-10px);
    transition: 0.5s;
  }

  &.taskCard-exit {
    height: 100px;
    transform: translateY(-10px);
  }

  &.taskCard-exit-active {
    height: 0;
    transform: translateY(-40px);
    transition: 0.5s;
  }
`;

const TaskWrapper = styled(Columns)`
  ${Responsiveness}
  background-color: ${COLOR.WHITE};
  color: ${COLOR.BLUE};
  padding: 10px;
  border: 1px solid ${COLOR.BLUE};
  border-radius: 10px;
  z-index: ${Z_INDEX.TASK_HEADER};
`;

const ToolbarWrapper = styled(Columns)`
  border-left: 1px solid ${COLOR.BLUE};
  margin-left: 15px;
`;

const TimeWrapper = styled(Block)`
  padding: 5px 10px;
  border-radius: 15px;
  background-color: ${COLOR.BLUE};
  color: ${COLOR.WHITE};
`;

export const TaskItem: FC<IProps> = ({ task }) => {
    const [descToggle, setDescToggle] = useState<boolean>(false);

    const changeToggle = () => {
        setDescToggle(prevState => !prevState);
    }

    const dates = map(task.notificationTimes, getDateFromStr);

    const countDiff = (date: Date): number => {
        const now = new Date();
        return date.getTime() - now.getTime();
    }

    const passedDates = sortBy(filter(dates, (date) => countDiff(date) <= 0), countDiff);
    const sortedTimes = sortBy(filter(dates, (date) => countDiff(date) > 0), countDiff);

    const dispatch = useDispatch();

    const handleDeleteClick = (e: any) => {
        e.stopPropagation();
        dispatch(removeTask(task._id));
    }

    const handleEditClick = (e: any) => {
        e.stopPropagation();
        dispatch(addModal(MODAL_TYPE.TASK_EDIT, {
            task: {
                ...task,
                notificationTimes: map(dates, formatDate),
            }
        }));
    }

    return (
        <Rows
            width="100%"
            margin="0 0 10px 0"
        >
            <TaskWrapper width="100%" onClick={changeToggle}>
                <Columns width="100%">
                    <Columns
                        width="100%"
                        alignY="center"
                        alignX="between"
                    >
                        <Block>
                            {task.name}
                        </Block>
                        <TimeWrapper>
                            <Text type={TextTypes.R16}>
                                {formatDate(first(sortedTimes) || last(passedDates))}
                            </Text>
                        </TimeWrapper>
                    </Columns>
                    <ToolbarWrapper>
                        <Rows
                            margin="0 0 0 10px"
                            alignY="center"
                            onClick={handleEditClick}
                        >
                            <ReactIcon
                                name="RiSettings3Fill"
                                size="18px"
                            />
                        </Rows>
                        <Rows
                            margin="0 0 0 10px"
                            alignY="center"
                            onClick={handleDeleteClick}
                        >
                            <ReactIcon
                                name="FaTrashAlt"
                                size="18px"
                            />
                        </Rows>
                    </ToolbarWrapper>
                </Columns>
            </TaskWrapper>
            <CSSTransition
                in={descToggle}
                timeout={{ enter: TIMEOUT.MODAL.ENTER.MS, exit: TIMEOUT.MODAL.EXIT.MS }}
                classNames="taskCard"
                unmountOnExit
            >
                <TaskCard>
                    <Text type={TextTypes.R16}>
                        {task.desc}
                    </Text>
                </TaskCard>
            </CSSTransition>
        </Rows>
    );
}
