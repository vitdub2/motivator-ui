import {FC, useEffect, useState} from "react";
import {IForm} from "../../../../auth/ui/components/modal/AuthForm";
import {useForm} from "react-hook-form";
import {TASK_CREATE_FORM} from "../../../store/constants";
import {FormButton, FormInput} from "../../../../../common/myControls/FormInputs/FormInputs";
import {RULES} from "../../../../../common/myControls/Form/constants";
import {Rows} from "../../../../../common/myControls/Grid/Grid";
import map from "lodash/map";
import size from "lodash/size";
import pullAt from "lodash/pullAt";
import styled from "styled-components";
import slice from "lodash/slice";
import forEach from "lodash/forEach";
import isUndefined from 'lodash/isUndefined'
import {MASK} from "../../../../../store/constants";
import {TaskDto} from "../../../store/interfaces";

const FormWrapper = styled(Rows)`
  margin-bottom: 10px;
`;

const TimeWrapper = styled(Rows)`
  max-height: 20vh;
  overflow: auto;
  &::-webkit-scrollbar {
    display: none;
  }
`;

interface ITaskForm extends IForm {
    defaultValues?: TaskDto,
}

export const TaskForm: FC<ITaskForm> = ({
    submit,
    loading,
    defaultValues
}) => {
    const [notificationTimes, setNotificationTimes] = useState<string[]>(defaultValues?.notificationTimes || [""]);

    const { handleSubmit, register, formState: {errors}, setValue, watch } = useForm({
        reValidateMode: 'onChange',
        defaultValues
    });

    useEffect(() => {
        if (defaultValues && defaultValues.notificationTimes) {
            forEach(defaultValues.notificationTimes, (time: string, index) => {
                setValue(`notificationTimes.${index}`, time)
            });
        }
    }, [defaultValues, setValue]);

    return (
        <form onSubmit={handleSubmit(submit)} autoComplete="off" id={TASK_CREATE_FORM}>
            <FormWrapper>
                <FormInput
                    name="name"
                    rules={RULES.REQUIRED.ANY}
                    register={register}
                    errors={errors}
                    placeholder="Название"
                    watch={watch}
                    setValue={setValue}
                    onChange={(data: string) => setValue("name", data)}
                />
                <FormInput
                    name="desc"
                    rules={RULES.REQUIRED.ANY}
                    register={register}
                    errors={errors}
                    placeholder="Описание"
                    watch={watch}
                    setValue={setValue}
                    onChange={(data: string) => setValue("desc", data)}
                />
                <TimeWrapper>
                    {map(notificationTimes, (notificationTime, index) => {
                        const name = `notificationTimes.${index}`;

                        const isValid = (time: string) => !isUndefined(time) && time.length === 5 && time.includes(':');

                        const onAdd = () => {
                            const arr = [...notificationTimes, undefined];
                            //@ts-ignore
                            setNotificationTimes(arr);
                            //@ts-ignore
                            forEach(arr, (item, index) => {
                                //@ts-ignore
                                setValue(`notificationTimes.${index}`, arr[index]);
                            })
                        }

                        const onDelete = () => {
                            const arr = [...notificationTimes];
                            pullAt(arr, [index]);
                            setNotificationTimes(arr);
                            forEach(arr, (item, index) => {
                                setValue(`notificationTimes.${index}`, arr[index]);
                            })
                        }

                        const onChange = (notificationTime: string) => {
                            if (isValid(notificationTime) && !notificationTimes.includes(notificationTime)) {
                                const leftSlice = slice(notificationTimes,0, index);
                                const rightSlice = slice(notificationTimes,index + 1);
                                setNotificationTimes([...leftSlice, notificationTime, ...rightSlice]);
                                //@ts-ignore
                                setValue(name, notificationTime);
                            }
                        }

                        return (
                            <FormInput
                                key={name + notificationTime}
                                name={name}
                                rules={RULES.REQUIRED.ANY}
                                register={register}
                                errors={errors}
                                placeholder="Время"
                                withAdd={index === size(notificationTimes) - 1 && isValid(notificationTime)}
                                withDelete={size(notificationTimes) > 1}
                                onAdd={onAdd}
                                onDelete={onDelete}
                                onChange={onChange}
                                mask={MASK.TIME}
                                watch={watch}
                            />
                        );
                    })}
                </TimeWrapper>
            </FormWrapper>
            <Rows width="100%" alignX="center">
                <FormButton
                    disabled={loading}
                    label={defaultValues ? 'Изменить' : 'Создать'}
                />
            </Rows>
        </form>
    );
}
