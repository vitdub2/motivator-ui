export type ErrorCreationError1C =
    "ACCOUNT_NOT_FOUND" |
    "ACCOUNT_NOT_ACTIVATED" |
    "SERVICE_CONTRACT_REQUIRED" |
    "PENDING_REWARDS_EXISTS" |
    "ACCRUALS_NOT_FOUND" |
    "ACCRUALS_LESS_1000" | "TEMPORARY"
;

export interface IError {
    error: ErrorCreationError1C;
    reason: string;
}
