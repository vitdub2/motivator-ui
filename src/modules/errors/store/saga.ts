import {all, takeLatest} from "redux-saga/effects";
import isEqual from "lodash/isEqual";
import get from "lodash/get";

import {appError} from "./ducks";

//Сага обработки ошибок
function* appErrorSaga(action: ReturnType<typeof appError>) {
  try {
    const error = action.payload;

    if (error instanceof Error) {
      const { message } = error;

      const status = get(error, "response.status");
      if (status) {
        //Ошибки общения с сервером
      }

      if (isEqual(message, "Network Error")) {
        //Ошибка сети
      }
    } else {
      yield console.log("Неизвестная ошибка:", error);
    }
  } catch (e) {
    yield console.log("error", e);
  }
}

export function* watchErrorsActions() {
  yield all([
      takeLatest(appError, appErrorSaga),
  ]);
}
