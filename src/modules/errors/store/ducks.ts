import { createAction, createReducer } from "redux-act";
import {rootHandlers} from "../../../store/rootHandlers";

export const appError = createAction("APP_ERRORS", error => error);

const defaultState = {};

export const errorsReducer = createReducer(rootHandlers(defaultState), defaultState);
