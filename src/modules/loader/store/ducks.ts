import {createAction, createReducer} from "redux-act";
import {createSelector} from "reselect";
import toString from "lodash/toString";
import get from "lodash/get";

import {IAppState} from "../../../store/interfaces";

import {rootHandlers} from "../../../store/rootHandlers";

export const showLoader = createAction<any>("SHOW_LOADER");

export const hideLoader = createAction<any>("HIDE_LOADER");

export const showGlobalLoader = createAction("SHOW_GLOBAL_LOADER");

export const hideGlobalLoader = createAction("HIDE_GLOBAL_LOADER");

export interface ILoaderState {
  [key: string]: boolean;
}

const defaultState: ILoaderState = {};

export const loaderReducer = createReducer<ILoaderState>(rootHandlers(defaultState), defaultState);

loaderReducer.on(showGlobalLoader, state => ({
  ...state,
  GLOBAL_LOADER: true
}));

loaderReducer.on(hideGlobalLoader, state => ({
  ...state,
  GLOBAL_LOADER: false
}));

loaderReducer.on(showLoader, (state, { type }) => ({
  ...state,
  [toString(type)]: true
  })
);

loaderReducer.on(hideLoader, (state, { type }) => ({
  ...state,
  [toString(type)]: false
}));

export const loaderStateSelector = (state: IAppState): IAppState["loader"] =>
  state.loader;

export const loaderSelector = (type: any) =>
    createSelector(loaderStateSelector, (loaderState: ILoaderState) =>
        get(loaderState, type, false)
    );
