import {
  differenceInSeconds,
  endOfMonth,
  format,
  formatISO,
  isSameDay,
  isSameMonth,
  parse,
  startOfMonth,
} from "date-fns";
import dateFnsLocalRu from "date-fns/locale/ru";
import { Map as ImmutableMap } from "immutable";
import {OrderedMap} from "immutable";
import axios from "axios";
import map from "lodash/map";
import set from "lodash/set";
import reduce from "lodash/reduce";
import flow from "lodash/flow";
import words from "lodash/words";
import join from "lodash/join";
import compact from "lodash/compact";
import assign from "lodash/assign";
import orderBy from "lodash/orderBy";
import toLower from "lodash/toLower";
import upperFirst from "lodash/upperFirst";
import keys from "lodash/keys";

import {
  REG_EXP,
} from "../store/constants";
import {
  ArrayDate,
  DateFns,
  DateFormatFns,
  Obj,
  PartialObj,
  PropertyName,
} from "../store/interfaces";

import { MouseEvent } from "react";

export const upperFirstCharOnly = flow([toLower, upperFirst]);

export const upperFirstCharWords = (value: string): string => {
  const tookWords = words(value, REG_EXP.WORDS_OR_BLANK);

  return reduce(
    tookWords,
    (acc, word) => `${acc}${upperFirstCharOnly(word)}`,
    ""
  );
};

const setAuthHeader = (token: any) => {
  axios.defaults.headers = {
    "Authorization": `Bearer ${token}`,
  };
};

const removeAuthHeader = () => {
  axios.defaults.headers = {};
};

const keyByMapOrdered = (items: any[], key: string): OrderedMap<string, any> => {
  let map = OrderedMap<string, any>();
  for (const item of items) {
    map = map.set(item[key], item);
  }
  return map;
};

const keyByMap = (items: any[], key: string): Map<string, any> => {
  const map = new Map();
  for (const item of items) {
    map.set(item[key], item);
  }
  return map;
};

const keyByImmutableMap = (
  items: any[],
  key: string
): ImmutableMap<string, any> => {
  return ImmutableMap(keyByMap(items, key));
};

export const keyByOrderedMap = (
    items: any[],
    key: string
): OrderedMap<string, any> => {
  return OrderedMap(keyByMapOrdered(items, key));
};

const putImmutableMap = (map: any, nodes: any[], key: string) => {
  return reduce(nodes, (acc, node) => acc.set(node[key], node), map);
};

export const setObj = <I extends object = object>(
  items: any[],
  key: string,
  acc?: Obj<string, I>
): Obj<string, I> =>
  reduce(items, (acc, item) => set(acc, item[key], item), acc || {});

const getValueWithFix = (
  prefix: string,
  value: string,
  postfix: string
): string => {
  return join(compact([prefix, value, postfix]), " ");
};

export {
  setAuthHeader,
  removeAuthHeader,
  keyByMap,
  keyByImmutableMap,
  putImmutableMap,
  getValueWithFix,
};

export const getPx = (obj: any) => assign(obj, { PX: `${obj.NUM}px` });

export const getDate = (date: DateFns, format?: DateFormatFns): Date => {
  if (date instanceof Date) {
    return date;
  }

  if (typeof date === "number" || (typeof date === "string" && !format)) {
    return new Date(date);
  }

  if (typeof date === "string" && format) {
    return parse(date, format, Date.now());
  }

  return new Date();
};

export const getDateByYear = (year: number): Date => {
  const prodDate = new Date();
  prodDate.setFullYear(year);
  prodDate.setMonth(0);
  prodDate.setDate(1);
  return prodDate;
}

export const getFormattedDate = (
  date: DateFns,
  formatDate: DateFormatFns
): string =>
  date ? format(new Date(date), formatDate, { locale: dateFnsLocalRu }) : "";

export const cutTimezone = (date : Date): Date => {
  let timezoneOffset = date.getTimezoneOffset() * 60000;
  return new Date(
      date.getTime() - timezoneOffset
  )
}

export const getDateWithoutTimezone = (rawDate: string | number, format : string | undefined = "dd.MM.yyyy") => {
  let date = getDate(rawDate, format);
  return cutTimezone(date).toISOString();
}

export const getISO = (date: DateFns, format?: DateFormatFns): string =>
  date ? formatISO(getDate(date, format)) : "";

export const getCurrentMonthRangeDate = (): Date[] => [
  startOfMonth(Date.now()),
  endOfMonth(Date.now()),
];

export const getFormattedRangeDate = (rangeDate?: Date[]): string => {
  if (!rangeDate) {
    return "";
  }

  const startDate = rangeDate[0];
  const endDate = rangeDate[1];

  if (
    isSameDay(startDate, startOfMonth(startDate)) &&
    isSameDay(endDate, endOfMonth(startDate))
  ) {
    return getFormattedDate(rangeDate[0], "LLLL");
  } else {
    const startDateFormat: DateFormatFns = isSameMonth(startDate, endDate)
      ? "d"
      : "d MMMM";

    const formattedStartDate = getFormattedDate(startDate, startDateFormat);
    const formattedEndDate = getFormattedDate(endDate, "d MMMM");

    return `${formattedStartDate} - ${formattedEndDate}`;
  }
};

export const sortArrayByDate = <A extends ArrayDate>(array: A[]): A[] =>
  orderBy(
    array,
    (item) => differenceInSeconds(new Date(), new Date(item.date)),
    "asc"
  );


export const getObjKey = <T extends PropertyName = PropertyName>(
  obj: Obj<T> | PartialObj<T>
): Obj<T, T> => {
  const result = keys(obj).reduce(
    (acc, item) => ({ ...acc, [item]: item }),
    {}
  );

  return result as Obj<T, T>;
};

export const getUrlWithParam = (url: string, param: string) =>
  `${url}/:${param}`;

export const getUrlWithUnnecessaryParam = (url: string, param: string) =>
    `${url}/:${param}?`;

export const getDefaultValuesUseForm = (defaultValues?: object) =>
  map(defaultValues, (value, key) => ({ [key]: value }));


// @ts-ignore
export const offsetContent = (e: MouseEvent<HTMLDivElement>) => {
  e.preventDefault();

  const currentTarget = e.currentTarget;
  const shiftX = e.clientX;
  const currentScrollLeft = currentTarget.scrollLeft;

  // @ts-ignore
  currentTarget.onmousemove = (event: MouseEvent) => {
    const difClientX = shiftX - event.clientX;
    currentTarget.scrollLeft = currentScrollLeft + difClientX;
  };

  const flushListeners = () => {
    currentTarget.onmouseleave = null;
    currentTarget.onmouseup = null;
    currentTarget.onmousemove = null;
  };

  // @ts-ignore
  currentTarget.onmouseup = (event: MouseEvent) => {
    const difClientX = shiftX - event.clientX;
    if (Math.abs(difClientX) > 5) {
      event.stopPropagation();
    }

    flushListeners();
  };

  currentTarget.onmouseleave = () => {
    flushListeners();
  };
};
