import React from "react";
import ReactDOM from 'react-dom';
import './index.css';
import {App} from './App';
import reportWebVitals from './reportWebVitals';
import Modal from "react-modal";

import { history, store } from "./store/createStore";
import { HashRouter as Router } from "react-router-dom";
import {Provider} from "react-redux";

import { initApp } from "./modules/auth/store/actions";

import {ConnectedRouter} from "connected-react-router";

Modal.setAppElement("#root");

store.dispatch(initApp());

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
        <Router>
            <App />
        </Router>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
