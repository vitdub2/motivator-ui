import {FC} from "react";
import {Redirect, Route, Switch } from "react-router-dom";
import {ConfirmPage} from "./modules/auth/ui/pages/ConfirmPage";
import {getUrlWithParam} from "./utils/helpers";
import {URLS} from "./store/constants";
import styled from "styled-components";
import {Rows} from "./common/myControls/Grid/Grid";
import {DevicesPage} from "./modules/devices/ui/pages/DevicesPage";

import {TasksPage} from "./modules/tasks/ui/pages/TasksPage";
import {MainPage} from "./common/layout/MainPage";

interface IProps {
    isAuthorized: boolean;
}

const MainWrapper = styled(Rows)`
  padding: 15px;
  width: 100%;
  height: 100%;
`;

interface IRouter {
    path: string;
    shouldRedirect: boolean;
    redirectPath?: string;
}

const AuthRouter: FC<IRouter> = ({
     shouldRedirect,
     children,
     path,
    redirectPath = URLS.MAIN
}) => {
    return (
        <Route path={path}>
            {!shouldRedirect ? (
                <>
                    {children}
                </>
            ) : (
                <Redirect to={redirectPath}/>
            )}
        </Route>
    )
}

export const Main: FC<IProps> = ({
    isAuthorized
}) => {
    return (
        <MainWrapper alignY="center" alignX="center">
            <Switch>
                <Route path={getUrlWithParam(URLS.CONFIRM, "confirmCode")}>
                    <ConfirmPage/>
                </Route>
                <AuthRouter
                    path={URLS.DEVICES}
                    shouldRedirect={!isAuthorized}
                >
                    <DevicesPage/>
                </AuthRouter>
                <AuthRouter
                    path={URLS.TASKS}
                    shouldRedirect={!isAuthorized}
                >
                    <TasksPage/>
                </AuthRouter>
                <AuthRouter
                    path={URLS.MAIN}
                    shouldRedirect={isAuthorized}
                    redirectPath={URLS.TASKS}
                >
                    <MainPage/>
                </AuthRouter>
                <Route>
                    {isAuthorized ? (
                        <Redirect to={URLS.TASKS}/>
                    ) : (
                        <Redirect to={URLS.MAIN}/>
                    )}
                </Route>
            </Switch>
        </MainWrapper>
    );
}
