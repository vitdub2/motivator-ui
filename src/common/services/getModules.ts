import camelCase from "lodash/camelCase";

export const getModules = (
  requireContext: any
): {
  [key: string]: string;
} =>
  requireContext.keys().reduce((acc: any, value: any) => {
    const domain = value.match(/([^/]*)\.[^.]*$/)[1];

    return {
      ...acc,
      [camelCase(domain)]: requireContext(value)
    };
  }, {});
