export const blue = "#4971d0";
export const blueDark = "#405aa0";
export const black = "#000";
export const black05 = "rgba(0, 0, 0, 0.5)";
export const green = "#27aa28";
export const yellow = "#f8ef26";
export const white = "#fff";
export const tomato = "#f52323";

export const greyF0 = "#f0f0f0";
export const greyDb = "#dbdbdb";
export const greyC0 = "#c0c0c0";
export const grey6b = "#6b6b6b";
export const grey8b = "#8b8b8b";

export const COLOR = {
  WHITE: "#fff",
  BLACK: "#000",
  BLUE_B1: "#b1dbff",
  BLUE: "#5680E9",
  BLUE_DARK: "#405aa0",
  GREEN: "#27aa28",
  YELLOW: "#f8ef26",
  ORANGE: "#FF6700",
  TOMATO: "#f52323",
  GREY_F0: "#f0f0f0",
  GREY_DB: "#dbdbdb",
  GREY_C0: "#c0c0c0", //цвет неативности
  GREY_6B: "#6b6b6b",
  GREY_8B: "#8b8b8b",
  GRAY_464B52: "#464B52",
  NONE: "rgba(0, 0, 0, 0)",
  OPACITY: {
    TWO: "rgba(0, 0, 0, 0.2)",
    FIVE: "rgba(0, 0, 0, 0.5)",
    SEVEN: "rgba(0, 0, 0, 0.7)",
    NINE: "rgba(0, 0, 0, 0.9)"
  },
  GRAY_MAIN: "#f7f7f7",
  GRAY_C8: "#C8C8C8",
  TEXT_GRAY: "#9598a1",
  TEXT_BLACK: "#333333",
  BLUE_29: "#002996",
};

export const thin = "400";
export const regular = "500";
export const bold = "600";

export const TEXT_WEIGHT = {
  THIN: "400",
  REGULAR: "500",
  BOLD: "600"
};

export const SHADOW = {
  DEFAULT: "0 0 6px 0 rgba(0, 0, 0, 0.2)",
  MAIN: "0 0 8px 4px #efefef",
  BOTTOM: "0 4px 3px 0 rgba(0, 0, 0, 0.2)",
  LINE_BOTTOM: "0 1px 0 0 rgba(0, 0, 0, 1)",
  OFFERWALL: "0px 4px 26px rgba(0, 0, 0, 0.06)",
  FOCUSED: `0 0 0 2px ${COLOR.BLUE_B1}`,
};

export const BORDER = {
  DEFAULT: `1px solid ${COLOR.GREY_C0}`,
  LIGHT_GREY: `1px solid ${COLOR.GREY_DB}`,
  GRAY: `1px solid ${COLOR.TEXT_GRAY}`,
  MAIN_BLUE: `1px solid ${COLOR.BLUE}`
};
