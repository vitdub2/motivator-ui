export const Z_INDEX = {
  MODAL: 10,
  TASK_HEADER: 5,
  TASK_DESC: 2,
};

export const DEFAULT = {
  PADDING: { NUM: 15, PX: "15px" },
  MARGIN: { NUM: 15, PX: "15px" }
};
