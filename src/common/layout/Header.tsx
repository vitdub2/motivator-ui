import {FC} from "react";
import {Block, Columns} from "../myControls/Grid/Grid";
import {Button} from "../myControls/Buttons/Button";
import styled from "styled-components";
import {COLOR} from "../consts/styles";
import {Text} from "../myControls/Text/Text";
import {TextTypes} from "../myControls/Text/interfaces";
import {ReactIcon} from "../myControls/ReactIcon/ReactIcon";
import {userSelector} from "../../modules/auth/store/selectors";
import {useDispatch, useSelector} from "react-redux";
import {getUser, signOut} from "../../modules/auth/store/actions";
import {loaderSelector} from "../../modules/loader/store/ducks";
import {addModal} from "../../modules/modals/store/actions";
import {MODAL_TYPE, URLS} from "../../store/constants";
import {NavLink, useHistory} from "react-router-dom";
import {Responsiveness} from "../myControls/constants";

interface IProps {
    isAuthorized: boolean;
}

const HeaderWrapper = styled.header`
  width: 100%;
  background-color: ${COLOR.BLUE};
  padding: 10px 15%;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 62px;
`;

const LogoWrapper = styled(Columns)`
  ${Responsiveness}
`;

const HeaderLink = styled(NavLink)`
  margin-right: 20px;
  display: flex;
  align-items: center;
  text-decoration: none;
  color: ${COLOR.WHITE};
  position: relative;
  
  &.active {
    &:after {
      position: absolute;
      content: '';
      width: 100%;
      height: 1px;
      background-color: ${COLOR.WHITE};
      bottom: 5px;
    }
  }
`;

export const Header: FC<IProps> = ({
   isAuthorized
}) => {
    const user = useSelector(userSelector);
    const isFetchingUser = useSelector(loaderSelector(getUser));

    const dispatch = useDispatch();

    const signInClick = () => {
        dispatch(addModal(MODAL_TYPE.AUTH));
    }

    const registerClick = () => {
        dispatch(addModal(MODAL_TYPE.REGISTER));
    }

    const signOutClick = () => {
        dispatch(signOut());
    }

    const history = useHistory();

    const redirectToMain = () => {
        history.push(URLS.MAIN)
    }

    return (
        <HeaderWrapper>
            <LogoWrapper alignY="center" onClick={redirectToMain}>
                <Block margin="0 15px 0 0">
                    <ReactIcon name="BsClock" color={COLOR.WHITE} size="32px"/>
                </Block>
                <Text type={TextTypes.B20} color={COLOR.WHITE}>
                    Motivator
                </Text>
            </LogoWrapper>
            <Columns>
                {isAuthorized ? (
                    <Columns>
                        <Columns margin="0 20px 0 0">
                            <HeaderLink activeClassName="active" key="tasks" to={URLS.TASKS}>
                                Мои активности
                            </HeaderLink>
                            <HeaderLink activeClassName="active" key="devices" to={URLS.DEVICES}>
                                Мои устройства
                            </HeaderLink>
                        </Columns>
                        <Columns alignY="center">
                            <Text
                                type={TextTypes.B18}
                                color={COLOR.WHITE}
                                id="header_username"
                            >
                                {user?.username}
                            </Text>
                            <Block margin="0 10px 0 15px">
                                <ReactIcon
                                    name={isFetchingUser ? "VscLoading" : "CgProfile"}
                                    color={COLOR.WHITE}
                                    size="24px"
                                />
                            </Block>
                            <Button
                                id="header_logout_btn"
                                click={signOutClick}
                                type="secondary"
                                label="Выйти"
                            />
                        </Columns>
                    </Columns>
                ) : (
                    <>
                        <Button
                            click={signInClick}
                            type="secondary"
                            label="Вход"
                            id="header_sign_in_btn"
                        />
                        <Block
                            margin="0 0 0 10px"
                        >
                            <Button
                                click={registerClick}
                                type="secondary"
                                label="Регистрация"
                                id="header_register_btn"
                            />
                        </Block>
                    </>
                )}
            </Columns>
        </HeaderWrapper>
    )
}
