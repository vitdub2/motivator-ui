import React, {FC} from "react";
import {Block, Columns, Rows} from "../myControls/Grid/Grid";
import {Text} from "../myControls/Text/Text";
import styled from "styled-components";
import {COLOR} from "../consts/styles";
import {Responsiveness} from "../myControls/constants";
import {useDispatch} from "react-redux";
import {addModal} from "../../modules/modals/store/actions";
import {MODAL_TYPE} from "../../store/constants";
import map from 'lodash/map';
import {Button} from "../myControls/Buttons/Button";

const ReasonBlock = styled(Columns)`
  font-size: 18px;
  background-color: transparent;
  border: 1px solid ${COLOR.BLUE};
  color: ${COLOR.BLUE};
  padding: 10px;
  border-radius: 5px;
  
  ${Responsiveness}
`;

const GridBlocks = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 10px;
  margin-bottom: 40px;
`;

export const MainPage: FC = () => {
    const dispatch = useDispatch();
    const openRegister = () => {
        dispatch(addModal(MODAL_TYPE.REGISTER));
    }
    const reasons = [
        'Слишком мало двигаетесь?',
        'Забываете про важные вещи?',
        'Забываете делать перерывы?',
        'Надоели будильники?',
    ]
    return (
        <Columns alignX="center">
            <Rows width="75%">
                <Rows margin="0 0 40px 0" alignX="center">
                    <Text type="B24">
                        Добро пожаловать
                    </Text>
                </Rows>
                <GridBlocks>
                    {map(reasons, (reason, index) => {
                        return (
                            <ReasonBlock
                                key={index}
                                onClick={openRegister}
                            >
                                {reason}
                            </ReasonBlock>
                        );
                    })}
                </GridBlocks>
                <Rows alignX="center">
                    <Block margin="0 0 40px">
                        <Text type="R18">
                            Пользуясь нашим приложением вы будете получать уведомления о том,
                            что пора встать и немного позаниматься!
                        </Text>
                    </Block>
                    <Block margin="0 0 40px">
                        <Text type="R18">
                            Пользуясь нашим приложением вы будете получать уведомления о том,
                            что пора встать и немного позаниматься!
                        </Text>
                    </Block>
                </Rows>
                <Rows alignX="center">
                    <Button
                        type="primary"
                        label="Регистрация"
                        click={openRegister}
                    />
                </Rows>
            </Rows>
        </Columns>
    );
}
