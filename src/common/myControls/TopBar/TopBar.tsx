import React, { FC } from "react";
import styled from "styled-components";

import { Block, Columns } from "../Grid/Grid";
import { Text } from "../Text/Text";

import { COLOR, SHADOW } from "../../consts/styles";
import { TOP_BAR } from "./constants";
import { TextTypes } from "../Text/interfaces";


type TopBarType = "arrow" | "cancel";

interface IDefaultTopBar {
  void?: boolean;
  noShadow?: boolean;
  padding?: string;
}

export const DefaultTopBar = styled.div<IDefaultTopBar>`
  position: relative;
  height: ${TOP_BAR.HEIGHT.PX};
  padding: 0 20px;

  background-color: ${COLOR.WHITE};
  box-shadow: ${SHADOW.MAIN};

  ${(p) => p.padding && `padding: ${p.padding};`}
  
  ${(p) => p.noShadow && `box-shadow: none;`}

  ${(p) =>
    p.void &&
    `
    height: auto;
    padding: 0;
  `}
`;

interface IWrapperTopBar {
  left?: any;
  middle: any;
  right?: any;
}

export const WrapperTopBar: FC<IWrapperTopBar & IDefaultTopBar> = ({
  left,
  middle,
  right,
  noShadow,
  padding,
}) => {
  return (
    <DefaultTopBar noShadow={noShadow} padding={padding}>
      <Columns alignY="center" height="100%">
        <Block
          position="absolute"
          width={!middle ? `100%` : ``}
        >
          <Columns alignY="center" width={!middle ? `100%` : ``}>
            {left}
          </Columns>
        </Block>

        {middle && (
          <Columns alignX="center" flex="1">
            {middle}
          </Columns>
        )}

        <Block position="absolute" right="0">
          <Columns alignY="center">{right}</Columns>
        </Block>
      </Columns>
    </DefaultTopBar>
  );
};

interface ITitle {
  title: string;
  titleClick?: () => void;
}

interface IWithSearch {
  onValueChange?: (value: string) => void;
  onCancel?: () => void;
  placeholder?: string;
  withoutSearch?: boolean;
  disableSearch?: boolean;
}

interface IWithWarning {
  withWarning?: boolean;
}

interface IClick {
  click?: () => void;
  closeLabel?: string;
}

export const Title: FC<ITitle> = ({ title, titleClick }) => (
  <Columns alignY="center">
    <Text type={TextTypes.M16} onClick={titleClick}>
      {title}
    </Text>
  </Columns>
);

const ButtonCancel: FC<IClick> = ({ click, closeLabel }) => (
  <Text
    type={TextTypes.R16}
    color={COLOR.TOMATO}
    onClick={click}
    clickable={!!click}
  >
    {closeLabel ? closeLabel : "Отмена"}
  </Text>
);

interface IMainTop {
  withSupport?: boolean;
  withBack?: any;
  withWallet?: boolean;
  closeClick?: any;
}
