import React, {FC} from "react";
import styled from "styled-components";
import {Block, Columns, Rows} from "../Grid/Grid";
import {COLOR} from "../../consts/styles";
import {Text} from "../Text/Text";
import {TextTypes} from "../Text/interfaces";
import {None, Responsiveness} from "../constants";
import {ReactIcon} from "../ReactIcon/ReactIcon";
import MaskedInput from 'react-text-mask';

interface IInputProps {
    name: string;
    rules: any,
    placeholder?: string;
    register: any;
    errors: any;
    type?: string;
    onChange?: (value: string) => void;
    defaultValue?: string;
    withAdd?: boolean;
    onAdd?: () => void;
    withDelete?: boolean;
    onDelete?: () => void;
    mask?: any;
    watch?: any;
    setValue?: any;
}

interface IError {
    isError: boolean;
}

const Input = styled(MaskedInput)<IError>`
  border: none;
  border-bottom: 1px solid ${COLOR.TEXT_GRAY};
  background-color: transparent;
  outline: transparent;
  padding: 5px;
  font-size: 18px;
  margin-bottom: 5px;
  ${p => p.isError && `
    border-color: ${COLOR.TOMATO};
  `}
`;

const InputWrapper = styled(Rows)`
  margin-bottom: 10px;
  position: relative;
`;

const ToolbarWrapper = styled(Columns)`
  position: absolute;
  top: 0;
  right: 0;
`;

const IconWrapper = styled(Block)`
  ${Responsiveness}
`;

export const FormInput: FC<IInputProps> = ({
    name,
    rules,
    placeholder,
    register,
    errors,
    type = "text",
    onChange,
    withAdd = false,
    onAdd,
    withDelete= false,
    onDelete,
    mask = false,
    watch,
}) => {
    const fieldErrors = errors[name];

    const handleChange = (e: any) => {
        const value = e.target.value;
        onChange && onChange(value.toString().trim());
    }

    const value = watch && watch(name);

    return (
        <InputWrapper>
            <Input
                {...register(name, rules)}
                placeholder={placeholder}
                type={type}
                autoComplete="off"
                isError={fieldErrors}
                onKeyUp={handleChange}
                mask={mask}
                guide={false}
                keepCharPositions={true}
                value={value}
            />
            <ToolbarWrapper>
                {withAdd && (
                    <IconWrapper onClick={() => onAdd && onAdd()}>
                        <ReactIcon
                            name="AiOutlinePlus"
                            size="21px"
                            color={COLOR.BLUE}
                        />
                    </IconWrapper>
                )}
                {withDelete && (
                    <IconWrapper
                        margin="0 0 0 10px"
                        onClick={() => onDelete && onDelete()}
                    >
                        <ReactIcon
                            name="AiOutlineCloseCircle"
                            size="21px"
                            color={COLOR.BLUE}
                        />
                    </IconWrapper>
                )}
            </ToolbarWrapper>
            {fieldErrors && (
                <Text type={TextTypes.R14} color={COLOR.TOMATO}>
                    {fieldErrors.message}
                </Text>
            )}
        </InputWrapper>
    );
}

interface IDisabled {
    disabled: boolean;
}

const ButtonWrapper = styled.button<IDisabled>`
  ${None} 
  border-radius: 25px;
  background-color: ${COLOR.BLUE};
  color: ${COLOR.WHITE};
  width: fit-content;
  padding: 5px 15px;
  font-size: 18px;
  ${Responsiveness}
  
  ${p => p.disabled && `
    background-color: ${COLOR.TEXT_GRAY};
  `}
`;

interface IButtonProps extends IDisabled{
    label: string;
    id?: string;
}

export const FormButton: FC<IButtonProps> = ({
    label,
    disabled,
    id,
}) => {
    return (
        <ButtonWrapper id={id} disabled={disabled}>
            {label}
        </ButtonWrapper>
    )
}
