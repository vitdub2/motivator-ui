import React, {FC} from 'react';
import loadable from '@loadable/component';

import {VscLoading} from 'react-icons/vsc';
import styled from "styled-components";
import {CursorPointer, Responsiveness} from "../constants";

interface IProps {
    name: string;
}

interface IIcon {
    color?: string;
    size?: string;
    button?: boolean;
}

const ReactIcons = loadable.lib(() => import(/* webpackPrefetch: true */ 'react-icons/all'), {
    fallback: (<VscLoading/>),
})

const Icon: FC<IProps> = ({ name }) => {
    return (
        //@ts-ignore
        <ReactIcons>
            {(icons: any) => {
                //@ts-ignore
                const icon = icons[name];
                //@ts-ignore
                return icon && icon();
            }}
        </ReactIcons>
    );
}

const Wrapper = styled.div<IIcon>`
  display: flex;
  ${p => p.color && `
    color: ${p.color};
  `}
  ${p => p.size && `
    font-size: ${p.size};
  `}
  ${p => p.button && `
    ${CursorPointer}
    ${Responsiveness}
  `}
`;

export const ReactIcon: FC<IProps & IIcon> = ({ name, color, size, button }) => {
    return (
        <Wrapper color={color} size={size} button={button}>
            <Icon name={name}/>
        </Wrapper>
    )
}
