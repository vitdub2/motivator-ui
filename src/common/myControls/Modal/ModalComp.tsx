import React, {
  Dispatch,
  FC,
  SetStateAction,
  useCallback,
  useEffect,
  useState
} from "react";

import { ContentA, IContent, IOverlay, OverlayA } from "./index";
import { Block } from "../Grid/Grid";

interface IProps {
  toggle: boolean;
  setToggle: Dispatch<SetStateAction<boolean>>;
  closeModal: () => void;
  content?: IContent;
  overlay?: IOverlay;
}

export const ModalComp: FC<IProps> = ({
  toggle,
  setToggle,
  closeModal,
  content,
  overlay,
  children
}) => {
  const [nodeChildrenHeight, setNodeChildrenHeight] = useState<number>(0);

  const nodeChildrenRef = useCallback(nodeChildren => {
    if (nodeChildren) {
      setNodeChildrenHeight(nodeChildren.getBoundingClientRect().height);
    }
  }, []);

  useEffect(() => {
    setToggle(true);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <OverlayA toggle={toggle} closeModal={closeModal} overlay={overlay}>
      <ContentA
        toggle={toggle}
        content={{
          height: content?.height || `${nodeChildrenHeight}px` || "100%",
          ...content
        }}
      >
        <Block ref={nodeChildrenRef} height={content?.height || "100%"}>
          {children}
        </Block>
      </ContentA>
    </OverlayA>
  );
};
