export const TIMEOUT = {
  MODAL: {
    ENTER: { S: "0.3s", MS: 300, FUNCTION: "ease-out" },
    EXIT: { S: "0.3s", MS: 300, FUNCTION: "linear" }
  }
};
