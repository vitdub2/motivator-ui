import React, { Dispatch, FC, SetStateAction, useCallback } from "react";

import { DefaultTopBar } from "../TopBar/TopBar";
import { Block } from "../Grid/Grid";

import {COLOR} from "../../consts/styles";

export const ModalDefaultBody: FC = ({
  children
}) => {
  return (
    <Block
      upright
    >
      {children}
    </Block>
  );
};

interface IHeaderProps {
  setStateAction: Dispatch<SetStateAction<number>>;
  notFilled?: boolean;
}

export const ModalDefaultHeader: FC<IHeaderProps> = ({
  setStateAction,
  children,
  notFilled
}) => {
  const applicationDescHeaderRef = useCallback(applicationDescHeaderElement => {
    if (applicationDescHeaderElement) {
      setStateAction(
        applicationDescHeaderElement.getBoundingClientRect().height
      );
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const backgroundColor = notFilled ? COLOR.GREY_F0 : COLOR.WHITE;

  return (
    <DefaultTopBar ref={applicationDescHeaderRef} void>
      <div style={{backgroundColor}}>
        {children}
      </div>
    </DefaultTopBar>
  );
};
