import React, { FC } from "react";
import styled from "styled-components";
import { CSSTransition } from "react-transition-group";

import { COLOR, greyF0 } from "../../consts/styles";
import { TIMEOUT } from "./constants";
import { Z_INDEX } from "../../consts/constants";
import {IphonePadding} from "../constants";

type BorderRadius = "top" | "bottom" | "all";

export interface IOverlay {
  noShadow?: boolean;
  zIndex?: number;
}

const Overlay = styled.div<IOverlay>`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;

  z-index: ${p => p.zIndex ? p.zIndex : Z_INDEX.MODAL};

  &.shadow-enter {
    background-color: ${COLOR.NONE};
  }

  &.shadow-enter-active {
    transition: ${TIMEOUT.MODAL.ENTER.FUNCTION} ${TIMEOUT.MODAL.ENTER.S};
    background-color: ${p => (p.noShadow ? COLOR.NONE : COLOR.OPACITY.SEVEN)};
  }

  &.shadow-enter-done {
    background-color: ${p => (p.noShadow ? COLOR.NONE : COLOR.OPACITY.SEVEN)};
  }

  &.shadow-exit {
    background-color: ${p => (p.noShadow ? COLOR.NONE : COLOR.OPACITY.SEVEN)};
  }

  &.shadow-exit-active {
    transition: ${TIMEOUT.MODAL.EXIT.FUNCTION} ${TIMEOUT.MODAL.EXIT.S};
    background-color: ${COLOR.NONE};
  }
`;

export interface IContent {
  top?: number | string;
  borderRadius?: BorderRadius;
  height?: string;
  maxWidth?: string;
}

const Content = styled.div<IContent>`
  position: absolute;
  height: fit-content;
  right: 0;
  left: 0;
  top: 0;
  bottom: 0;
  
  padding: 15px;
  
  ${p => p.maxWidth && `
    max-width: ${p.maxWidth};
  `}
  width: fit-content;
  max-width: 400px;
  margin: auto;

  background-color: ${greyF0};

  border-radius: 25px;

  //нужен чтобы работал border-radius, почему - хз
  overflow: hidden;

  &.up-enter {
    transform: translateY(${p => p.height});
  }

  &.up-enter-active {
    transition: ${TIMEOUT.MODAL.ENTER.FUNCTION} ${TIMEOUT.MODAL.ENTER.S};
    transform: translateY(0);
  }

  &.up-enter-done {
    transform: translateY(0);
  }

  &.up-exit {
    transform: translateY(0);
  }

  &.up-exit-active {
    transition: ${TIMEOUT.MODAL.EXIT.FUNCTION} ${TIMEOUT.MODAL.EXIT.S};
    transform: translateY(${p => p.height});
  }
  
  ${IphonePadding}
`;

interface IOverlayA {
  toggle: boolean;
  closeModal: () => void;
  overlay?: IOverlay;
  id?: string;
}

export const OverlayA: FC<IOverlayA> = ({
  toggle,
  closeModal,
  overlay,
  children
}) => {
  return (
    <>
      <CSSTransition
        in={toggle}
        timeout={{ enter: TIMEOUT.MODAL.ENTER.MS, exit: TIMEOUT.MODAL.EXIT.MS }}
        classNames="shadow"
      >
        <Overlay id="shadow" onClick={closeModal} {...overlay}>
          {children}
        </Overlay>
      </CSSTransition>
    </>
  );
};

interface IContentA {
  toggle: boolean;
  content: IContent;
}

export const ContentA: FC<IContentA> = ({ toggle, content, children }) => {
  return (
    <>
      <CSSTransition
        in={toggle}
        timeout={{ enter: TIMEOUT.MODAL.ENTER.MS, exit: TIMEOUT.MODAL.EXIT.MS }}
        classNames="up"
      >
        <Content onClick={e => e.stopPropagation()} {...content}>
          {children}
        </Content>
      </CSSTransition>
    </>
  );
};
