import { COLOR, SHADOW } from "../consts/styles";
import { DEFAULT } from "../consts/constants";
import { css } from "styled-components";

export const Desktop = "@media (min-width: 500px)";
export const Mobile = "@media (max-width: 500px)";

export const VisuallyHidden = `
  position: absolute;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  height: 1px;
  width: 1px;
  margin: -1px;
  padding: 0px;
  border: 0;
  clip: rect(0 0 0 0);
  overflow: hidden;
`;

export const MainStyleBox = `
  background-color: ${COLOR.WHITE};
  box-shadow: ${SHADOW.MAIN};
`;

export const CursorPointer = `
  ${Desktop} {
    cursor: pointer;
  }
`;

export const Responsiveness = `
  ${Desktop} {
    cursor: pointer;
    &:hover {
      opacity: 0.8;
    }
    
    &:focus {
      box-shadow: ${SHADOW.FOCUSED};
    }
  }
  
  &:active {
    opacity: 0.8;
  }
`;

export const None = `
  border: none;
  outline: none;
  width: 100%;

  padding: 0px;
  margin: 0px;

  background-color: transparent;
`;

export interface IClickable {
  clickable?: boolean;
}

export const Clickable = css<IClickable>`
  ${p => p.clickable && Responsiveness}
`;

export interface IIsDisabled {
  isDisabled?: boolean;
}

export const DefaultScrollBody = `
  padding-bottom: 65px;
  margin-bottom: 55px;
  overflow-y: auto;
`;

export const FontRoboto = `
  font-family: 'Roboto', sans-serif;
`;

export const InputFont = `
  ${FontRoboto};
  color: ${COLOR.TEXT_BLACK};
  
  ::placeholder {
    color: ${COLOR.TEXT_GRAY};
  }
`;

export type DefaultType = "default" | string;

export interface IDefault {
  margin?: DefaultType;
  padding?: DefaultType;
}

export const Default = css<IDefault>`
  margin: ${p => (p.margin === "default" ? DEFAULT.MARGIN.PX : p.margin)};
  padding: ${p => (p.padding === "default" ? DEFAULT.PADDING.PX : p.padding)};
`;

export const IphonePadding = `
  padding-bottom: max(0px, calc(env(safe-area-inset-bottom) / 3));
`;
