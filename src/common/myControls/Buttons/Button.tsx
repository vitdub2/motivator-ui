import React, { FC, MouseEvent } from "react";
import styled from "styled-components";

import {FontRoboto, None, Responsiveness} from "../constants";
import { COLOR } from "../../consts/styles";
import { TextWeightRoboto } from "../Text/interfaces";

type ButtonType = "primary" | "secondary" | "text" | "exit" | "delete";

interface IWrapper {
  kind: ButtonType;
  size?: "small" | "mini";
  deactivated?: boolean;
  borderRadius?: string;
  shadow?: string;
}

interface IHeight {
  height?: string;
}

const Wrapper = styled.button<IWrapper & IHeight>`
  ${Responsiveness};
  ${FontRoboto};
  ${None};

  display: flex;
  align-items: center;
  justify-content: center;
  
  width: fit-content;
  
  padding: 10px 15px;
  
  border-radius: 25px;
  border: 2px solid;

  font-size: 16px;
  font-weight: ${TextWeightRoboto.Medium};
  letter-spacing: 1px;
  white-space: nowrap;
  
  ${p => p.borderRadius && `
    border-radius: ${p.borderRadius};
  `}
  
  ${p => p.shadow && `
    box-shadow: ${p.shadow};
  `}

  ${p =>
    p.kind === "primary" &&
    `
      color: ${COLOR.WHITE};
      background-color: ${COLOR.BLUE};
      border-color: ${COLOR.BLUE};
  `}

  ${p =>
    p.kind === "secondary" &&
    `
      color: ${COLOR.BLUE};
      background-color: ${COLOR.WHITE};
      border-color: ${COLOR.BLUE};
  `}
    
  ${p =>
    p.kind === "text" &&
    `
    color: ${COLOR.BLUE};
    background-color: transparent;
    border-color: transparent;
  `}
  
  ${p => p.kind === "exit" && `
    color: ${COLOR.TOMATO};
    background-color: transparent;
    border-color: ${COLOR.TOMATO};
  `}

  ${p => p.kind === "delete" && `
    background-color: ${COLOR.TOMATO};
    color: ${COLOR.WHITE};
  `}
    
  ${p =>
    p.size === "small" &&
    `
      height: 40px;
      font-size: 14px;
      text-transform: none;
  `}
  
  ${p =>
    p.size === "mini" &&
    `
      height: 32px;
      font-size: 11px;
      text-transform: none;
  `}
  
  &:disabled {
    color: ${COLOR.GREY_C0};
    background-color: ${COLOR.GREY_8B};
    border-color: ${COLOR.GREY_8B};

    opacity: 1;
    cursor: default;
  }
  
  ${p =>
    p.deactivated && `
      background-color: ${COLOR.GREY_8B};
      border-color: ${COLOR.GREY_8B};
      cursor: pointer;
    `
  }
  
  ${p => p.color && `
    background-color: ${p.color};
  `}
`;

interface IProps {
  type: ButtonType;
  size?: "small" | "mini";
  label?: string;

  form?: string;
  submit?: boolean;

  disabled?: boolean;
  deactivated?: boolean;

  borderRadius?: string;
  shadow?: string;
  color?: string;

  click?(e?: MouseEvent<HTMLButtonElement>): void;

  stopPropagation?: boolean;

  id?: string;
}

export const Button: FC<IProps & IHeight> = ({
  type,
  size,
  label,
  submit,
  form,
  click,
  disabled,
  stopPropagation,
  deactivated,
  borderRadius,
  shadow,
  height,
  color,
  id,
}) => {
  const handleClick = (e: MouseEvent<HTMLButtonElement>) => {
    if (stopPropagation) {
      e.stopPropagation();
    }

    if (click) {
      click();
    }
  };

  return (
    <Wrapper
      shadow={shadow}
      kind={type}
      size={size}
      type={submit ? "submit" : "button"}
      form={form}
      onClick={handleClick}
      disabled={disabled}
      deactivated={deactivated}
      borderRadius={borderRadius}
      height={height}
      color={color}
      id={id}
    >
      {label}
    </Wrapper>
  );
};
