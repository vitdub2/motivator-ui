import React, { FC } from "react";
import styled from "styled-components";
import { CSSTransition } from "react-transition-group";

import { ITimeout } from "./interfaces";

interface IHeightWrapper {
  timeout: ITimeout;
  heightStart: string;
  heightEnd: string;
}

const HeightWrapper = styled.div<IHeightWrapper>`
  ${p => `
  &.h-enter {
    height: ${p.heightStart};
  }
  
  &.h-enter-active {
    transition: ${p.timeout.FUNCTION}
      ${p.timeout.S};
    height: ${p.heightEnd};
  }

  &.h-enter-done {
    height: ${p.heightEnd};
  }

  &.h-exit {
    height: ${p.heightEnd};
  }

  &.h-exit-active {
    transition: ${p.timeout.FUNCTION}
      ${p.timeout.S};
    height: ${p.heightStart};
  }
  
  &.h-exit-done {
    height: ${p.heightStart};
  }
  
`}
`;

interface IProps {
  toggle: boolean;
}

export const HeightAnim: FC<IProps & IHeightWrapper> = ({
  toggle,
  timeout,
  heightStart,
  heightEnd,
  children
}) => {
  return (
    <>
      <CSSTransition in={toggle} timeout={timeout.MS} classNames="h">
        <HeightWrapper
          timeout={timeout}
          heightStart={heightStart}
          heightEnd={heightEnd}
        >
          {children}
        </HeightWrapper>
      </CSSTransition>
    </>
  );
};
