import React, { FC } from "react";
import { CSSTransition } from "react-transition-group";
import { ITimeout } from "./interfaces";
import styled from "styled-components";

interface IWrapper {
  timeout: ITimeout;
  opacityStart?: number;
  opacityEnd?: number;
}

const Wrapper = styled.div<IWrapper>`
  ${p => `
  &.op-enter {
    opacity: ${p.opacityStart};
  }
  
  &.op-enter-active {
    transition: ${p.timeout.FUNCTION}
      ${p.timeout.S};
    opacity: ${p.opacityEnd};
  }

  &.op-enter-done {
    opacity: ${p.opacityEnd};
  }

  &.op-exit {
    opacity: ${p.opacityEnd};
  }

  &.op-exit-active {
    transition: ${p.timeout.FUNCTION}
      ${p.timeout.S};
    opacity: ${p.opacityStart};
  }
  
  &.op-exit-done {
    opacity: ${p.opacityStart};
  }
`}
`;

interface IProps {
  toggle: boolean;
}

export const OpacityAnim: FC<IProps & IWrapper> = ({
  toggle,
  timeout,
  opacityStart = 0,
  opacityEnd = 1,
  children
}) => {
  return (
    <>
      <CSSTransition
        in={toggle}
        timeout={timeout.MS}
        classNames="op"
        mountOnEnter
        unmountOnExit
      >
        <Wrapper
          timeout={timeout}
          opacityStart={opacityStart}
          opacityEnd={opacityEnd}
        >
          {children}
        </Wrapper>
      </CSSTransition>
    </>
  );
};
