export interface ITimeout {
  S: string;
  MS: number;
  FUNCTION: string;
}