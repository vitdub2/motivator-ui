import React, { FC } from "react";
import { CSSTransition } from "react-transition-group";
import styled from "styled-components";

import { ITimeout } from "./interfaces";

interface IRotateWrapper {
  degreesRotate: string;
  timeout: ITimeout;
}

const RotateWrapper = styled.div<IRotateWrapper>`
  ${p => `
  &.rotate-enter-active {
    transition: ${p.timeout.FUNCTION}
      ${p.timeout.S};
    transform: rotate(${p.degreesRotate}deg);
  }

  &.rotate-enter-done {
    transform: rotate(${p.degreesRotate}deg);
  }

  &.rotate-exit {
    transform: rotate(${p.degreesRotate}deg);
  }

  &.rotate-exit-active {
    transition: ${p.timeout.FUNCTION}
      ${p.timeout.S};
    transform: rotate(0deg);
  }
`}
`;

interface IProps {
  toggle: boolean;
  deg: string;
  timeout: ITimeout;
  direction?: boolean;
}

export const RotateAnim: FC<IProps> = ({
  toggle,
  deg,
  timeout,
  direction = true,
  children
}) => {
  const degreesRotate = direction ? deg : `-${deg}`;

  return (
    <>
      <CSSTransition in={toggle} timeout={timeout.MS} classNames="rotate">
        <RotateWrapper degreesRotate={degreesRotate} timeout={timeout}>
          {children}
        </RotateWrapper>
      </CSSTransition>
    </>
  );
};
