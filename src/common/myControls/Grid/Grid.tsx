import styled from "styled-components";
import {
  Default,
  DefaultScrollBody,
  IDefault,
  MainStyleBox,
  Responsiveness
} from "../constants";

type Position = "absolute" | "relative";

type Height = "inherit" | "100%" | "-webkit-fill-available" | string;

type AlignYRow = "top" | "center" | "bottom" | "between" | "around" | "evenly";
type AlignXRow = "left" | "center" | "right";

type AlignYColumn = "top" | "center" | "bottom";
type AlignXColumn =
  | "left"
  | "center"
  | "right"
  | "between"
  | "around"
  | "evenly";

interface IBlockPosition {
  position?: Position;
  top?: string;
  left?: string;
  right?: string;
  bottom?: string;
  zIndex?: number;
  allZero?: boolean;
}

export interface IBlock {
  float?: "left" | "right" | "normal";

  height?: Height;
  minHeight?: Height;
  maxWidth?: string;
  width?: string;

  background?: string;
  boxShadow?: string;
  border?: string;
  borderRadius?: string;

  horizontally?: boolean;
  upright?: boolean;

  mainStyle?: boolean;
  scrollBody?: boolean;
  responsiveness?: boolean;
}

export const Block = styled.div<IBlock & IBlockPosition & IDefault>`
  ${Default}

  box-sizing: border-box;
  
  position: ${p => p.position};
  top: ${p => p.top};
  left: ${p => p.left};
  right: ${p => p.right};
  bottom: ${p => p.bottom};
  z-index: ${p => p.zIndex};

  ${p =>
    p.allZero &&
    `top: 0; left: 0; right: 0; bottom: 0;
  `}
    
  ${p =>
    p.mainStyle &&
    `
    ${MainStyleBox}
  `}   
   
   ${p =>
     p.scrollBody &&
     `
    ${DefaultScrollBody}
  `}  
   
   ${p =>
     p.responsiveness &&
     `
    ${Responsiveness}
  `}  
  
  float: ${p => p.float};
  
  height: ${p => p.height};
  min-height: ${p => p.minHeight};
  max-width: ${p => p.maxWidth};
  width: ${p => p.width};

  background: ${p => p.background};

  box-shadow: ${p => p.boxShadow};

  border: ${p => p.border};

  border-radius: ${p => p.borderRadius};

  ${p =>
    p.upright &&
    `
    overflow-y: auto;
  `}
  
  ${p =>
    p.horizontally &&
    `
    display: flex;
    overflow-x: overlay;
    overflow-y: hidden;
  `}
`;

interface IRow {
  direction?: "reverse" | "direct";
  flex?: string;

  alignX?: AlignXRow;
  alignY?: AlignYRow;

  marginBottom?: string;
}

export const Rows = styled(Block)<IRow & IBlock>`
  display: flex;
  position: relative;

  flex-direction: column;
  ${p => p.direction === "reverse" && "flex-direction: column-reverse"};

  flex: ${p => p.flex};

  ${p => p.alignX === "left" && `align-items: flex-start`};
  ${p => p.alignX === "center" && `align-items: center`};
  ${p => p.alignX === "right" && `align-items: flex-end`};

  ${p => p.alignY === "top" && `justify-content: flex-start`};
  ${p => p.alignY === "center" && `justify-content: center`};
  ${p => p.alignY === "bottom" && `justify-content: flex-end`};
  ${p => p.alignY === "between" && `justify-content: space-between`};
  ${p => p.alignY === "around" && `justify-content: space-around`};
  ${p => p.alignY === "evenly" && `justify-content: space-evenly`};

  ${p =>
    p.marginBottom &&
    `
    &:not(:last-child):not(:empty) {
      margin-bottom: ${p.marginBottom};
    }
  `}
`;

export interface IColumn {
  direction?: "reverse" | "direct";
  flex?: string;

  alignX?: AlignXColumn;
  alignY?: AlignYColumn;

  flexWrap?: boolean;

  marginRight?: string;
}

export const Columns = styled(Block)<IColumn & IBlock>`
  display: flex;
  position: relative;

  flex-direction: row;
  ${p => p.direction === "reverse" && "flex-direction: row-reverse"};

  flex: ${p => p.flex};

  ${p => p.alignX === "left" && `justify-content: flex-start`};
  ${p => p.alignX === "center" && `justify-content: center`};
  ${p => p.alignX === "right" && `justify-content: flex-end`};
  ${p => p.alignX === "between" && `justify-content: space-between`};
  ${p => p.alignX === "around" && `justify-content: space-around`};
  ${p => p.alignX === "evenly" && `justify-content: space-evenly`};

  ${p => p.alignY === "top" && `align-items: flex-start`};
  ${p => p.alignY === "center" && `align-items: center`};
  ${p => p.alignY === "bottom" && `align-items: flex-end`};

  ${p => p.flexWrap && `flex-wrap: wrap`};

  ${p =>
    p.marginRight &&
    `
    &:not(:last-child):not(:empty) {
      margin-right: ${p.marginRight};
    }
  `}
 
`;

type GridItem = "start" | "end" | "center" | "stretch";

type GridContent =
  | "start"
  | "end"
  | "center"
  | "stretch"
  | "space-around"
  | "space-between"
  | "space-evenly";

interface IGrid {
  template?: string;
  gap?: string;

  alignItems?: GridItem;
  justifyItems?: GridItem;
  alignContent?: GridContent;
  justifyContent?: GridContent;
}

export const Grid = styled(Block)<IGrid & IBlock>`
  display: grid;

  ${p => p.template && `grid-template: ${p.template};`}

  ${p => p.gap && `gap: ${p.gap};`}
  
  ${p => p.alignItems && `align-items: ${p.alignItems};`}
  ${p => p.justifyItems && `justify-items: ${p.justifyItems};`}
  
  ${p => p.alignContent && `align-content: ${p.alignContent};`}
  ${p => p.justifyContent && `justify-content: ${p.justifyContent};`}
`;
