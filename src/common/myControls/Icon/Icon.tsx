import React, { FC, MouseEvent, useEffect, useState } from "react";
import styled from "styled-components";

import { getModules } from "../../services/getModules";
import { Clickable, IClickable, IIsDisabled, None } from "../constants";
import { COLOR } from "../../consts/styles";

//@ts-ignore
export const icons = getModules(require.context("./assets", true));

interface ISize {
  size?: {
    height: string;
    width?: string;
  };
}

interface IColor {
  color?: string;
  src?: string;
}

const Size = (p: any) =>
  p.size && `width: ${p.size.width}; height: ${p.size.height};`;

const Color = (p: any) => `
  background: ${p.color || COLOR.BLACK};
  
  -webkit-mask-image: url(${p.src});
  -webkit-mask-repeat: no-repeat;
  -webkit-mask-size: contain;
  -webkit-mask-position: center;
`;

const Disable = (p: any) =>
  p.isDisabled &&
  `
    background-color: ${COLOR.TEXT_GRAY};
  `;

const Img = styled.img<ISize>`
  ${Size}
`;

const IconColor = styled.div<ISize & IColor & IIsDisabled>`
  ${Size}
  ${Color}
  ${Disable}
`;

const IconButton = styled.button<ISize & IColor & IClickable & IIsDisabled>`
  ${None}
  
  ${Size}
  ${Color}
  ${Clickable}
  ${Disable}
`;

interface IProps {
  name?: string;

  click?(e?: MouseEvent<HTMLButtonElement>): void;
}

export const Icon: FC<IProps & ISize & IColor & IIsDisabled> = ({
  src,
  name,
  color,
  size = { height: "24px" },
  isDisabled,
  click
}) => {
  const [iconSrc, setIconSrc] = useState<string>();

  if (size) {
    size.width = size.width || size.height;
  }

  useEffect(() => {
    setIconSrc((src || (name && icons[name])) as string);
  }, [src, name]);

  if (click && iconSrc) {
    return (
      <IconButton
        src={iconSrc}
        area-label={name || "icon"}
        size={size}
        color={color}
        onClick={click}
        clickable={!!click}
        isDisabled={isDisabled}
      />
    );
  }

  if ((color || isDisabled) && iconSrc) {
    return (
      <IconColor
        src={iconSrc as string}
        area-label={name || "icon"}
        size={size}
        color={color}
        isDisabled={isDisabled}
      />
    );
  }

  return iconSrc ? (
        <Img src={iconSrc} alt={name || "icon"} size={size} />
      ) : (
         <></>
      );
};
