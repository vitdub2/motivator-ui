import React, {FC} from "react";
import styled from "styled-components";

import {Rows} from "../Grid/Grid";

import {Clickable, IClickable} from "../constants";
import {COLOR} from "../../consts/styles";
import {INotificationText, ITextMod, ITextType, TextTypes, TextWeightRoboto} from "./interfaces";

export const Text = styled.span<ITextType & ITextMod & IClickable>`
  text-decoration: ${p => p.textDecoration};
  word-wrap: ${p => p.wordWrap};
  line-height: ${p => p.lineHeight};
  text-align: ${p => p.textAlign};
  opacity: ${p => p.opacity};
  word-spacing: ${p => p.wordSpacing};
  word-break: ${p => p.wordBreak};
  white-space: ${p => p.whiteSpace};

  ${p => {
    if (typeof p.type === "string") {
      const weight = p.type.substr(0,1);
      const size = +p.type.substr(1);
      const trueWeight = 
          weight === "R" ? TextWeightRoboto.Regular
              : weight === "B" ? TextWeightRoboto.Bold
              :  TextWeightRoboto.Medium
      return `
        font-size: ${size}px;
        font-weight: ${trueWeight};
      `;
    }
  } }
  
  ${p =>
    p.type === TextTypes.R10 &&
    `
    font-size: 10px;
    font-weight: ${TextWeightRoboto.Regular};
  `}
  
  ${p =>
    p.type === TextTypes.R12 &&
    `
    font-size: 12px;
    font-weight: ${TextWeightRoboto.Regular};
  `}

  ${p =>
    p.type === TextTypes.R14 &&
    `
    font-size: 14px;
    font-weight: ${TextWeightRoboto.Regular};
  `}

  ${p =>
    p.type === TextTypes.R14G &&
    `
    font-size: 14px;
    font-weight: ${TextWeightRoboto.Regular};
    color: ${COLOR.TEXT_GRAY};
  `}

  ${p =>
    p.type === TextTypes.R16 &&
    `
    font-size: 16px;
    font-weight: ${TextWeightRoboto.Regular};
  `}

  ${p =>
    p.type === TextTypes.R16G &&
    `
    font-size: 16px;
    font-weight: ${TextWeightRoboto.Regular};
    color: ${COLOR.TEXT_GRAY};
  `}
  
  ${p =>
    p.type === TextTypes.M14 &&
    `
    font-size: 14px;
    font-weight: ${TextWeightRoboto.Medium};
  `}
  
  ${p =>
    p.type === TextTypes.M14G &&
    `
    font-size: 14px;
    font-weight: ${TextWeightRoboto.Medium};
    color: ${COLOR.TEXT_GRAY};
  `}
  
  ${p =>
    p.type === TextTypes.M16 &&
    `
    font-size: 16px;
    font-weight: ${TextWeightRoboto.Medium};
  `}
  
  ${p =>
    p.type === TextTypes.M18 &&
    `
    font-size: 18px;
    font-weight: ${TextWeightRoboto.Medium};
  `}
  
  ${p =>
    p.type === TextTypes.M20 &&
    `
    font-size: 20px;
    font-weight: ${TextWeightRoboto.Medium};
  `}

  ${p =>
    p.type === TextTypes.B12 &&
    `
    font-size: 12px;
    font-weight: ${TextWeightRoboto.Bold};
  `}
  
  ${p =>
    p.type === TextTypes.B14 &&
    `
    font-size: 14px;
    font-weight: ${TextWeightRoboto.Bold};
  `}
  
  ${p =>
    p.type === TextTypes.B16 &&
    `
    font-size: 16px;
    font-weight: ${TextWeightRoboto.Bold};
  `}  
  
  ${p =>
    p.type === TextTypes.B18 &&
    `
    font-size: 18px;
    font-weight: ${TextWeightRoboto.Bold};
  `}
  
  ${p =>
    p.type === TextTypes.B20 &&
    `
    font-size: 20px;
    font-weight: ${TextWeightRoboto.Bold};
  `}
  
  ${p =>
    p.type === TextTypes.Inherit &&
    `
    font-size: inherit;
    font-weight: inherit;
    color: inherit;
  `}
  
  ${p => p.shouldWrap && `
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  `}
  
  ${p => p.customWidth && `
    width: ${p.customWidth};
  `}
  
  ${p => p.color && `color: ${p.color};`}
  ${p => p.weight && `font-weight: ${p.weight};`}
  ${p => p.italic && `font-style: italic;`}
  
  ${Clickable};
`;

export const BBold = styled.b`
  font-weight: ${TextWeightRoboto.Bold};
`;

export const NotificationText: FC<INotificationText> = ({
  text,
  padding = "20px",
  textAlign = "center",
  children,
  type= TextTypes.R16G
}) => (
  <Rows alignX={textAlign} padding={padding}>
    <Text type={type} textAlign={textAlign} whiteSpace='pre-wrap'>
      {text}
      {children}
    </Text>
  </Rows>
);

export const NotificationNoInfo: FC<INotificationText> = props => (
  <NotificationText text="Нет информации" {...props} />
);

export const A = styled.a`
  color: ${COLOR.BLUE};

  &:visited {
    color: ${COLOR.TEXT_GRAY};
  }
`;
