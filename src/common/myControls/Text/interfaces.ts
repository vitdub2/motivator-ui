type ITextAlign = "center" | "left" | "right";

export enum TextWeightRoboto {
  Regular = "400",
  Medium = "500",
  Bold = "700"
}

export enum TextTypes {
  R10 = "R10",
  R12 = "R12",
  R14 = "R14",
  R14G = "R14G",
  R16 = "R16",
  R16G = "R16G",
  M14 = "M14",
  M14G = "M14G",
  M16 = "M16",
  M18 = "M18",
  M20 = "M20",
  B12 = "B12",
  B14 = "B14",
  B16 = "B16",
  B18 = "B18",
  B20 = "B20",
  Inherit = 'Inherit'
}

export interface ITextType {
  type: TextTypes | string;
  italic?: boolean;
}

export interface ITextMod {
  color?: string;
  weight?: TextWeightRoboto;
  textAlign?: ITextAlign;
  textDecoration?: string;
  lineHeight?: string;
  wordWrap?: string;
  opacity?: number;
  wordSpacing?: string;
  wordBreak?: 'normal' | 'break-all' | 'break-word';
  whiteSpace?: 'normal' | 'nowrap' | 'pre' | 'pre-line' | 'pre-wrap',
  shouldWrap?: boolean,
  customWidth?: string,
}

export interface INotificationText {
  text?: string;
  padding?: string;
  textAlign?: ITextAlign;
  type?: TextTypes;
}

