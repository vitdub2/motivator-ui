import validator from "email-validator";
import {
  addDays,
  differenceInDays,
  isFuture,
  isPast
} from "date-fns";
import mapValues from "lodash/mapValues";
import assign from "lodash/assign";
import isEmpty from "lodash/isEmpty";

import { REG_EXP } from "../../../store/constants";
import {
  getDate,
} from "../../../utils/helpers";

export const REQUIRED = {
  required: { value: true, message: "Обязательное поле" }
};

interface IRules {
  [key: string]: any;
}

export const RULES: IRules = {
  WORD: {
    minLength: { value: 2, message: "Слишком короткое название" },
    maxLength: 20,
    pattern: REG_EXP.WORD
  },
  WORDS: {
    maxLength: { value: 60, message: "Слишком длинное название" },
    pattern: REG_EXP.WORDS
  },
  EMAIL: {
    validate: (value: string) =>
      value ? validator.validate(value) || "Некорректный E-mail" : true
  },
  DATE: {
    pattern: {
      value: REG_EXP.DATE,
      message: "Формат даты: 01.01.2000"
    }
  },
  IS_FUTURE_DATE: {
    pattern: {
      value: REG_EXP.DATE,
      message: "Формат даты: 01.01.2000"
    },
    validate: (value: string) => {
      const oneDayAfterDate = addDays(getDate(value, "dd.MM.yyyy"), 1);
      return (
        isFuture(oneDayAfterDate) ||
        isEmpty(value) ||
        "Дата должна быть, не ранее текущей даты"
      );
    }
  },
  POLICY_DATE: {
    pattern: {
      value: REG_EXP.DATE,
      message: "Формат даты: 01.01.2000"
    },
    validate: (value: string) => {
      const date = getDate(value, "dd.MM.yyyy");
      const diff = differenceInDays(date, new Date()) + 1;
      return (
          diff >= 4 ||
          isEmpty(value) ||
          "Дата должна быть, не ранее чем через 4 дня от текущей даты"
      );
    }
  },
  IS_PAST_DATE: {
    pattern: {
      value: REG_EXP.DATE,
      message: "Формат даты: 01.01.2000"
    },
    validate: (value: string) =>
      isPast(getDate(value, "dd.MM.yyyy")) ||
      isEmpty(value) ||
      "Дата должна быть, не позднее текущей даты"
  },
  ANY: {},
  NUMBER: {
    pattern: {
      value: REG_EXP.NUMBER,
      message: "Допустимые символы: 0-9"
    }
  },
};

RULES.REQUIRED = mapValues(RULES, value => assign({}, value, REQUIRED));
