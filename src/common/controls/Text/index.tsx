import styled from "styled-components";
import {black05, blue, bold, green, grey6b, regular, tomato, white, yellow} from "../../consts/styles";

const defaultFont = `
font-family: "Montserrat", sans-serif;
`;

// Base text styles
const defaultH = `
  line-height: normal;
  position: relative;
  margin: 0;
  font-weight: ${bold};
`;

const H1 = styled("h1")`
  ${defaultH};
  font-size: 30px;
`;

const H2 = styled("h2")`
  ${defaultH};
  font-size: 24px;
`;

const H20 = styled("h3")`
  ${defaultH};
  font-size: 20px;
`;

const H16 = styled.h4`
  ${defaultH};
  font-size: 16px;
`;

const H14 = styled("h4")`
  ${defaultH};
  font-size: 14px;
`;

const defaultP = `
  line-height: normal;
  margin: 0;
`;

const P = styled.div`
  ${defaultP}
  margin: 0 0 10px;
`;

const P2 = styled.div`
  ${defaultP}
  color: ${grey6b};
`;

const P12 = styled.div`
  ${defaultP}
  font-size: 12px;
`;
// color: rgba(0, 0, 0, 0.5);

const P14 = styled.div`
  ${defaultP}
  font-size: 14px;
`;

const P16 = styled.div`
  ${defaultP}
  font-size: 16px;
`;

const P18 = styled.div`
  ${defaultP}
  font-size: 18px;
`;

// Modificators
const Greened = styled.p`
  ${defaultP}
  color: ${green};
`;

const Blued = styled.p`
  ${defaultP}
  color: ${blue};
`;

const Yellowed = styled.p`
  ${defaultP}
  color: ${yellow};
`;

const Whited = styled.p`
  ${defaultP}
  color: ${white};
`;

const Tomato = styled.p`
  ${defaultP}
  color: ${tomato};
`;

const Black05 = styled.p`
  ${defaultP}
  color: ${black05};
`;

const Grey6b = styled.p`
  ${defaultP}
  color: ${grey6b};
`;

interface ITextColor {
  textColor?: string;
}

const TextColor = styled.p<ITextColor>`
${defaultP}
  color: ${p => p.textColor};
`;

const BRegular = styled.b`
  font-weight: ${regular};
`;

export {
  defaultFont,
  H1,
  H2,
  H20,
  H14,
  H16,
  P,
  P2,
  P12,
  P14,
  P16,
  P18,
  Greened,
  Blued,
  Yellowed,
  Whited,
  Tomato,
  Black05,
  Grey6b,
  TextColor,
  BRegular
};

