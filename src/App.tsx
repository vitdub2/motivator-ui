import React, {FC, useEffect} from 'react';
import './App.css';
import firebase from 'firebase/compat/app';
import 'firebase/compat/messaging';
import * as dotenv from 'dotenv';
import Lockr from 'lockr';
import {useDispatch, useSelector} from "react-redux";
import {Modals} from "./modules/modals/ui/pages/Modals";
import {Main} from "./Main";
import {
  MOTIVATOR_FCM_TIMESTAMP,
  MOTIVATOR_FCM_TOKEN
} from "./modules/auth/store/constants";
import {isAuthenticatedSelector, userSelector} from "./modules/auth/store/selectors";
import {getUser} from "./modules/auth/store/actions";
import styled from "styled-components";
import {Rows} from "./common/myControls/Grid/Grid";
import {Header} from './common/layout/Header';

dotenv.config({path: '.env'});

const PageWrapper = styled(Rows)`
  background-color: white;
  height: 100%;
`;

export const App: FC = () => {
  const dispatch = useDispatch();

  const firebaseConfig = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_SENDER_ID,
    appId: process.env.REACT_APP_APP_ID,
    measurementId: process.env.REACT_APP_MEASUREMENT_ID
  };
  // Initialize Firebase
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  Notification.requestPermission((result) => {
    if (result === 'granted') {
      const token = Lockr.get(MOTIVATOR_FCM_TOKEN);
      const messaging = firebase.messaging();
      if (!token) {
        messaging.getToken().then((token: string) => {
          Lockr.set(MOTIVATOR_FCM_TOKEN, token);
          Lockr.set(MOTIVATOR_FCM_TIMESTAMP, new Date().getTime());
        }).catch(console.error);
      }

      messaging.onMessage((payload) => {
        const {notification} = payload;
        console.log(payload);
        navigator.serviceWorker.getRegistration().then((reg) => {
          const options = {
            body: notification?.body,
            // actions: [
            //   {action: 'A', title: 'A'},
            //   {action: 'B', title: 'B'}
            // ]
          }
          console.log(options);
          //@ts-ignore
          reg?.showNotification(notification?.title, options);
        })
      });
    }
  });

  const user = useSelector(userSelector);
  const isAuthorized = useSelector(isAuthenticatedSelector);

  useEffect(() => {
    if (!user && isAuthorized) {
      dispatch(getUser());
    }
  }, [dispatch, user, isAuthorized])

  return (
    <PageWrapper>
      <Header isAuthorized={isAuthorized}/>
      <Main isAuthorized={isAuthorized}/>
      <Modals />
    </PageWrapper>
  );
}
